Title: How to handle big plain log files.
Date: 2013-08-16 21:29
Tags: english, programming, hacks, logs, algorhithms, search
Category: python
Slug: how-to-handle-big-plain-log-files
Author: Francesco Romani
Summary: Techniques and experiences to efficiently manage big log files.


Intro
-----

[The night is dark and full of terrors](http://gameofthrones.wikia.com/wiki/Melisandre).
And legacy systems are always lurking in the night.
There is no shortage of logging libraries, techniques and approaches.
For a bunch of reasons, some good, some not, you can find yourself
entangled with plain old log files.
And maybe legacy systems.
In the night.

Maybe, you are stuck with, and you need to deal with, plain log files.
For a variety of reasons I am stuck as well: I routinely deal with
those big log files.
And for 'big' I mean fairly big: 2GB each/process/day is the baseline,
and roughly 4 GB each/process/day is quite common.
Multiply this for an handful tens of processes, and you quickly discover
a tide of data which raises quite rapidly.

I am going to describe a few techniques to deal with such files, given the
constraint that you cannot move to something more advanced
(for any definition of advanced) in the near future.
Until the sun will rise again, at least.

<!-- more -->

A (more) formal(ish) definition of log
--------------------------------------

For our purposes and for this article, and without loss of generality, a log file
can be defined as

* An [immutable](http://en.wikipedia.org/wiki/Persistent_data_structure) sequence, where immutable means
* Written only in append mode, thus new items are always added, never modified.
* Read 'infrequently', thus optimized for writing.
* [Rotated](http://en.wikipedia.org/wiki/Log_rotation) or otherwise archived without changed
  its properties. Most importantly, items are never (re)arranged or changed.
* Made of records. Each sequence item is a 'record'.
* Each record is plain text, often ASCII.
* Each record has variable length, but
* Each record has fixed format, meaning it is made by distinguished fields in fixed order.
* Contains a timestamp.

The assumption the log will not be changed or, most important, tampered, is quite strong;
anyway, dealing with malicious changes is a completely different problem which will not be
addressed here. As for accidental changes and/or corruptions, they are assumed to be negligibile
in frequency and impact (another fairly strong yet very common in practice assumption).

The final and key assumption is every log record contains a timestamp. Once again, in theory
this should not taken for granted; in practice, do *not* have such a timestamp is a recipe
for disaster, thus it is expected to be present.

Having such a timestamp is the pillar of any further processing, because it allows us to
easily map log entries to a timeline.


Access patterns
---------------

The definition of how the logs will be accessed (read) for later processing is critical as
well to efficiently manage them. I will outline an usage pattern from my own experience,
which I however find general enough. Then:

* Logs tend to be relatively short lived. You no longer care what appened more than
  a month ago.
* The unit of interest is a request/response operation: you want to inspect post-action
  how a given request was processed and why a given response was produced.
* You are thus interested on what happened on a specific, well determined time interval
* You may need to look at the same time interval across multiple processes (a request/response
  processing may require the cooperation of multiple processes).
* You need rarely read the logs this way (aka this is more for debugging than for
  accounting).

Summary: the most common usage pattern is: randomly jump somewhere in the record sequence
and fetch a sub-sequence of N records. This operation is performed relatively rarely.


Mapping to a timeline
---------------------

If we view a log as a sequence of timestamped records, the mapping of the sequence to a timeline
looks straightforward and convenient. And indeed is another pillar of efficient access.
However, there are some caveat which needs to be considered, and addressed.

1. A log record maps into a *logical* line which, much like a programming language
   statement, can be spread on more than one a *physical* line. However, we *cannot* assume there
   is a general and easy why to join all the physical lines _a posteriori_. In other words,
   We cannot assume we can easily detect the *logical* line boundaries.
2. We cannot assume a given timestamp exists. It is possible we want a time point on which
   nothing interesting happened; thus, such time point is effectively not-existent
   (in a nutshell the timeline is discrete and not homogeneous).
3. We cannot estimate any fixed mapping between time and offset due to point 2 above and due
   to impredictable logging activity. Maybe in a given interval nothing happened, maybe
   we faced a storm of events.

Point #3 above deserves a few more words.
Due to impredictable nature of logging (driven by the request load or by other external factors
out of our control) the mapping `time_delta -> space_delta` is absolutely impredictable.
Even assuming a fixed size for log records (already undoable), we cannot assume anything
about the frequency or the incidency of events. Thus we cannot expect, for example,
that if the time interval `9:00 -> 9:05` weights X MBs, then the time interval `9:05 -> 9:10`
weights the same, even roughly the same.


How to jump to a timepoint
--------------------------

If we can establish a sort of `time_stamp -> file_offset` mapping, from (fraction of) seconds
to bytes, we can easily jump around in the log file.

The atom in a log file, however, is not the byte, but is the record. But a log record,
for constraints outlined above, maps in a logical line of text.
Moreover, due to the point #1, we must work with *physical* line, and then we must deal with
lines which *do not* have a corresponding timestamp, because hey effectively are a part of a logical
line, which by definition does.

So, we need a function, aptly named `get_ts` which can extract a `Timestamp` ADT from a physical
line of text.


    def get_ts(tspec):
        """
        see above for details.
        You must implement a separate Timestamp ADT for each
        log file format you want to deal with.
        if the parameter is None, must provide an invalid Timestamp.
        """
        raise NotImplementedError


The `Timestamp` ADT must have the following properties:

* Must be comparable, in order to make the containing lines sortable.
* Must support an *invalid* value, much like the NULL pointer. An *invalid* value is different
  from any other value.
* Must support the concept of _roughly equal_, due to point #2 above.

Then:


    class Timestamp(object):
        """
        must support rich comparison, but also support `rough' equivalence
        and invalid values.
        """
        def __init__(self, spec):
            if spec is None:
                # invalid timespec.
                raise NotImplementedError
            else:
                # spec is a format-dependent line of text;
                # you need to parse it and extract the time value.
                raise NotImplementedError

        def equals(self, other, dist=1000):  # milliseconds
            raise NotImplementedError

        def __eq__(self, other):
            return self.equals(other, dist=0)

        def __gt__(self, other)
            raise NotImplementedError

        def __lt__(self, other)
            raise NotImplementedError

        def __nonzero__(self):
            return self.valid()

        @property
        def valid(self):
            raise NotImplementedError



The last point deserves an explanation.
We know a time point can be inexistent (no event logged in that instant); however, we cannot predict
its existence. When querying for an interval, we generally do not really care (well, not always)
about the *exact* boundaries of the interval we are interested in, granted the returned result
is greater or equal of the one requested. Summary: we can afford to deal with a slightly larger
response interval, but definitely we do not want to lose something.

There are plenty of strategies to deal with this requirement.
An effective one, and the one is implemented below is: go search, and stop when you are near enough.

That is the reason why we need an _approximately equal_ operator; the approximation factor must be,
of course, configurable.

We have now delimited all the perimeter of operation, so we can sketch out some code to
efficently reach a log record (approximately) corresponding to a given time point.

Givent that we established a satisfyng, yet approximate, `time_stamp <=> file_offset` mapping,
we can now leverage the intrinsic sorting of a log record and apply a binary search.
I purposely skip time skew (or time machine) troubles, because they just need to be sorted
out *ASAP*, or you can have much bigger problems than losing the ability of quickly jump around
in a log file.

What follows is the workhorse function, together with the support code.
the docstring and the inline comments should provide enough information, coupled with
all the context provided above in the article, to understand the purpose of the code.

A cautionary warning: this is really reconstructed code. May contain typos. The purpose
is to demostrate the algorithm(s), not to be pasted and used.
The actual, real, code will be on github/bitbcket at later time.

First, the obvious helpers

    _KB = 1 * 1024
    _MB = _KB * 1024


    def refresh(handle):
        """
        update the underlying log and block size.
        If you are working offline (e.g. already rotated log)
        you will not need this. Otherwise, when working with
        live (being written) data, you will likely need to
        refresh() to avoid to miss fresh data.
        """
        import os.path
        size = os.path.getsize(handle.name)
        block = min(size/2,  256 * _KB)  # euristically determined.
        return size, block


Then the workhorse function; `handle` is any file-like object.


    def seek_time(handle, target, get_ts, tries=23, linesep='\n'):
        """
        seeks by timespec instead of by position.

        `target' must be a valid Timestamp subclass.
        Bugs apart, a time-based seek can fail even in a well
        formed log file because of the nature of logging.
        If you specify a too narrow timestamp (often seconds is already
        too strict), you may fall in a hole.

        If nothing happened in the specified time instant,
        the log will not contain any entry and thus the seek can
        fail (worst case) or can degenerate in a slow linear search
        (best case, not yet fully implemented).

        Moreover, consider that even a careful linear search can fail
        if nothing at all happened in a time window.
        """
        size, block = refresh(handle)
        if not size:
            # we want to avoid unpossible yet stupid scenarios
            return False

        found = False
        count = 0
        start = 0
        end = size

        while not found and count < tries:
            count += 1

            pos = _pos(start, end, block)
            handle.seek(pos)

            current, pos = _sync_file(handle, block, get_ts, linesep)
            if current.equals(target):
                # wow, we are lucky
                found = True
            else:
                if current > target:
                    end = pos
                else:
                    start = pos

        handle.seek(pos)
        return found


And the needed helpers


    def _sync_data(data, get_ts, linesep='\n'):
        """
        finds the first valid timestamp in the provided data buffer.
        """
        current, syncpoint = get_ts(None), 0
        while not current:
            try:
                syncpoint = 1 + data.index(linesep, syncpoint)
                current = get_ts(data[syncpoint:])
            except ValueError:
                break
        return current, syncpoint


    def _sync_file(handle, size, get_ts, linesep='\n'):
        """
        read (and discards) blocks until finds the first valid
        timestamp.
        """
        current = get_ts(None)
        while not current:
            data = handle.read(size)
            current, syncpoint = _sync_data(data, get_ts, linesep)
        # get back to the beginning of the meaningful data
        pos = handle.tell() - block + syncpoint
        return current, pos


    def _pos(start, end, block):
        """
        compute the next seeking position.
        You need to get backward (at least) half a block size
        because FIXME
        """
        pos = (end + start)/2 - block/2
        return pos



Having code which allows to jump to any (reasonnable near value of) time in the timeline
log, we can now derive a more generic mapping function timespec -> file offset.
It is pretty trivial, just a matter of keeping track of the offset before the seek()
and restore it.


    def find_time(handle, target, get_ts, linesep='\n'):
        """
        convert a time spec in an absolute position.

        Raise ValueError if the time spec is not found.
        """
        base = handle.tell()
        found = seek_time(handle, target, get_ts, linesep)
        pos = handle.tell()
        handle.seek(base)
        if not found:
            raise ValueError("%s not found" % target)
        return pos


Trivial, even though the error concealement can be made stronger.


How to extract an interval
-------------------------- 

Once we can jump around easily in a log, extracting a time interval is
easier coded than said:


    def extract(handle, start, stop, get_ts, linesep='\n'):
        """
        extracts and returns as a single string block
        all the entries between two timestamps.
        """
        stop_pos = find_time(handle, stop, get_ts, linesep)
        seek_time(handle, start, get_ts, linesep)
        size = stop_pos - handle.tell()
        return handle.read(size)


The key here is seek a time interval twice, so the interval size in bytes
can be efficiently computed.
The careful reader will notice a subtle bug. The implementation of `seek_time`
actually errs toward earlier times, which is fine (and compliant to outline
requirements) if we are looking for the start of an interval (aka the lower time).
But if we are looking for the end of an interval (aka the higher time)
we have an high risk to lose some records.
The band-aid fix is just to extend the time interval to the right.


Outro
-----

Once carefully outlined the access patterns and the characteristics of
a log file, you can efficiently manage even in a VHLL like python.

Dealing with such files is definitely not rocket science, but do it properly
has enough (yet little) pitfalls to make it a non-trivial task, and definitely
more work than a ten-minutes exercise.

The algorithmic core is admittely trivial, a fair straightforward
binary search and basic file(-like) juggling.

However, in practice this kind of code is not exactly to be taken for granted,
especially in legacy systems. While simple, is much akin to the
[egg of Columbus](http://en.wikipedia.org/wiki/Egg_of_Columbus): looks simple,
yet few bother to write the code. Maybe it is deceptively simple?

The takeaway nugget: you can efficiently mangle big data files in plain python.
Just avoid to do a plain `for line in file` loop at all cost, because the wasteful
creation/destruction of string objects, most of them you do not care of,
will kill the performances.

#### EOF

