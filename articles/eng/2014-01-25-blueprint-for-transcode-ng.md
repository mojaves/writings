Title: A blueprint for next-generation transcode.
Date: 2014-01-25 09:45
Tags: english, programming, projects, ffmpeg, transcode
Category: projects
Slug: blueprint-for-transcode-ng
Author: Francesco Romani
Summary: A Blueprint of a possible transcode reboot.


Intro
-----

WARNING: this post will be updated. Current revision is: #2

I co-maintained the [transcode project](http://www.transcoding.org) from
mid-2005 to mid-2010. I quit as maintainer just because my interest
in video/media processing became cold, and I ended up being no longer
an user of transcode myself. I firmly believe in dogfooding, and not being
an user myself was very bad for the project.
When coding for transcode also started to be not longer funny,
I had to quit. Well, I an open source project you cannot really quit.
Is just I stopped sending and adding patches. The project basically
died the same day, because no one ever as take my place.

In open source world, this basically means transcode has become obsolete,
and its niche has been filled by other tools, most notably ffmpeg.

However, I still feel today there are some blind spots in the current
free software media processing stack.
There is some empty space around FFmpeg, a space which a reincarnation
of transcode may fill succesfully.

Over the years I slowly but constantly gathered my ideas about
what was fundamentally wrong and broken with the original transcode,
how to fix it and how to bring it -at least on paper- on the 2010s
and beyond.
In this post I collect and explain those points, and this will
be the new generation transcode (aka ngtranscode) blueprint.

<!-- more -->


Basic project management
------------------------

Altough I still like mercurial, git and github are nowadays major driving forces,
and they basically won the mindshare war.
The adoption of git and the presence on github will grant, basically for free,
a greater visibility, better networking opportuinities and will lower to the minimum
the barrier to contribute. Isn't everyone using git those days?

ngtranscode will be hosted on a git repo on github. This will also mark clearly
that while the project has its roots on transcode, it is a different and independent
entity. Project history will be imported to preserve credits and copyright;
license will be the same as original transcode (GPL v2).
The addition of a plugin/linking exception, if feasibile, is planned.


Removal of obsolete code
------------------------

Transcode shipped a fair amount of code, most notably in the import layer,
which was progressively made obsolete (mostly) by the evolving of ffmpeg.
The benefits of this code was already dubtious at time, except
for some well known use cases (v4l for instances).

In its core, transcode supported various operational modes, some of them
relics of its origins, others born (or grown) has hacks or workarounds
around the weaknesses of the import/export layer.

After the import on the new repo, all the code not clearly useful in
the core, support libraries and import/export layer will be removed. 


Reorganization of the binaries and of the user interface
--------------------------------------------------------

Transcode featured little-to-none difference between plumbing and porcelain,
and that lead to suboptimal outcome: internal helpers exposed to the user,
than increased burden to keep a decent compatibility, document them
and letting the usable on isolation.

In general the UI of transcode was also complex and convoluted, result
of accumulation of patches and fixes in the years and never really cleaned
due to compatibility constraints.

ngtranscode will feature a cleaner separation between plumbing and porcelain;
the gateway to the user will be a single entry point, maybe `ngtc`,
featuring subcommads like mercurial (or very modern git).

ngtranscode will be parallel installable easily and by default.
the UI of the gateway command will be regular and predictable, rewritten
from scratch.


Definition and enforcement of an API/ABI for plugins
----------------------------------------------------

transcode plugins were loadable parts of the application, not really independent
third party components. It was not possible to write third party component
in isolation. Well, it was not possible in an easy and straightforward way.

transcode didn't have a clear, well defined plugin ABI, and the plugin API
had holes. Plugins depended on various ways to single symbols exported
by accident or as an hack.

ngtranscode will have clear, well defined and well enforced API and ABI
for plugins, most notably flters.


New import layer
-----------------

transcode had two import threads, one for audio and one for video.
While this was probably driven by the structure of the import layer,
built around a `popen()`ed toolset pipeline (tccat, tcextact, tcdemux,
tcdecode) more than a design decision, this structure lead to a bunch
of problems:

* probing was done in a separate process.
  That consumed input data in an irreversible way!
  moreover, there it was not fd-passing between the probe (tcprobe)
  and the main converter process (transcode), leading to 
* the input source was opened at least THREE times independently.
  no good for special sources like V4L or DVB
* last but not least: A/V desync was hard to impoossible to fix or even
  detect; actually, this was mostly caused by the lack of proper
  timestamping, but the import layer structure hasn't helped either.

ngtranscode will have a much simpler and leaner import layer built around
to the ffmpeg import module.
Such module solves (or can solve easily) all the problems above, provides
much greater compatibility, and with no penalties since ffmpeg is already
ubiquitous, an hard dependency and least but not last transcode has not
a credible replacement except for specific use cases.

The new import layer MUST not, however, be limited to FFmpeg and must
feature am interface generic enough to allow other import sources,
maybe for specific tasks, to be built.


New export layer
----------------

During the transcode 1.2.0 cycle, never completed, an attempt was made
to introduce a new generic plugin API and a new export layer, split
into explicit encoder and multiplexor stages, each one with its own
plugins.

The new API, written and implemented  by yours truly, got some things
right but in the end it was a false step.
transcode 1.2.0 never took off partly due to this.

ngtranscode will drop mostly of this new code and restart from the transcode
1.1.7 classic export layer. Once again, the main focus will be the
ffmpeg export module. However, some ideas from the failed
1.2.0 attempt must be preserved and rearranged in some way.

As per the import layer, the export layer must allow other export sources.
The export layer must address since the beginning the problem of the
code duplication in the old export layer, which was one of the
biggest (and correct) driving forces behind the new module system
attempt.


LUA plugins
-----------

ngtranscode will allow filter plugins to be written in LUA.
ngtranscode will strongly recommend the [luajit](http://luajit.org)
VM, although the plain [LUA vm](http://www.lua.org) must be
supported as well.

A LUA plugin must be considered exactly equivalent, from the core
point of view, to any binary plugin.
The implementation language of a filter plugin must be considered
a fully transparent implementation detail, as it happens already
for binary plugins, because nothing actually prevents an user
motivated enough to write a plugin in some language different from C.

The supporto for LUA in filter plugins will just make the LUA
language an official, first class citizen for the implementation
of transcode filters, like (just) C was for transcode.


frei0r compatibility
--------------------

[frei0r](http://www.dyne.org/software/frei0r/) is both a simple API
and a plugin collections for video effects.
ngtranscode must support frei0r plugins and threat them as first class,
binary citizens.


Talk is cheap, show me the code
-------------------------------

The [repo is here](http://github.com/mojaves/ngtranscode). Don't hold
your breath, this is almost exclusively a pet peeve of mine, not
something with a schedule or a timetable. Patches welcome :)


### EOF

