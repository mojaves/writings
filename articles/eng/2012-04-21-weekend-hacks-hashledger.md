Title: Weekend Hacks - HashLedger
Date: 2012-04-21 12:08
Tags: english, Clang, python, weekendhacks, hashtable, skiplist, hack
Category: python
Slug: weekend-hacks-hashledger
Author: Francesco Romani
Summary: implementing skip lists and merging with hash tables for fun and profit.


Intro
-----

Long time no hacks. Too bad. I should really [never stop hackin'](http://rdegges.com/never-stop-hacking).
They say you should focus on just one thing at time. They are right. On the other hand, the brilliant
and never forgotten Richard Feynman reportedly said it is good for inspiration to have an handful open and
not-working-on-it-right-now items always loaded in your head, because inspiration can came suddenly and
can unexpectedly find new connection and strange paths. And it is always fun to have something to tinker
about. There is truth in that statement too, it is "just" hard to do right. :)
While tinkering with the SkipLists (yup, that's the topic of this spring) I stumbled in one of those
ideas which conglomerate a few concepts running in the background in my head.

<!-- more -->

Enter HashLedger: melting Hash Tables and Skip Lists for fun and profit.
------------------------------------------------------------------------

Hashes are great as online, volatile cache. [memcached](http://memcached.org/) and friends come rapidly to mind.
On my job, I use them extensively. However, hashes has an annoyance as they do not support range queries.
For the range queries, I mean something like "give me all the values of the key in the range [A, B]".
One can think: easy enough, just iterate in the list [A ... B] and do one get per item. So:


	result = [ hash_table[k] for k in range(A, B) ]


Or, if your python is rusty, in pseudo code (it's really python in disguise, however ;))


	result = list()
	for k in range(A, B):
	     result.append(hash_table.get(k))


And that's indeed just fine if you exactly know every item in the range you're querying.
For some reasons, it is possible (and indeed quite common in some use cases I deal daily with) you
**do not** actually know all the keys, but **just the first**, and you have to iteratively get the next
one until a certain condition is met.
Hashes of course  do not have the notion of "next item" since it makes little if any sense at all:
the next item in _which_ sense?

In order to support that kind of range query, you need to keep track of the ordering of the keys, and,
most importantly, to _establish_ an ordering by imposing (or requesting) a key comparison function.
Again, it's neither nothing new nor revolutionary (think for example about the python's `SortedSet`).
However, I've to say it's not that easy to find such a data structure implemented and made avalaible in C.
Too bad, because looks like in Java [there is something quite like](http://www.java2s.com/Code/Java/Collections-Data-Structure/ConcurrentSkipListMap.htm).
Maybe I haven't searched in the right places. No big deal however since thinking and implementing it
it's actually a lot of fun :)

One quick fix whould be to introduce the notion of `next item` in the hash itself, by adding some
references to the following item(s). I thought about this for a while but it basicallys sounds much
like to a poor man index. In other words, I have'nt (yet) found a clever way to do this **without being**
a poor man index. So, fall back to regular indexes.

We need all of those: `add`, `del` and `get` as fast as we can, and the index should (by the very requirement)
support the `next` and `previous` operation. A skim of the algorhythm literature leave us with no much
choice. A tree is usual a good solution... Unless you got a Skip List handy :)
The Skip List provides `O(logn)` search time for all the operations above, and that's good enough for
a first implementation.

Let's summarize what we got in the simpler case of just one index.
The extended hash + index construct, now known as **HashLedger** give us

- **get(K)** in `O(1)`: being a read-only operation, we can exploit the underlying hash itself.
- **add(K)** in `O(1) + O(logn) ~= O(logn)`: we must update the index, and that drags down a bit.
- **del(K)** in `O(1) + O(logn) ~= O(logn)`: we must update the index again, as above.
- **next(K)** in `O(logn)`: for the range query, we must use the index. Not too bad.
- **prev(K)** in `O(logn)`: ditto as above.

So far so good. But a question is still open: how do you compare keys?
I found just two sensible ways to compare the keys. I named then following the track of the comparison
procedure itself.

In the **external** comparison mode, the caller provides the comparison function as a callback, and the
HashLedger code calls it whenever needs it. This of course means the caller must know the structure
of the objects it puts into the HashLedger. In all honesty, I always assumed, sometimes unconsciously,
that the caller puts homogeneous content in the HashLedger, e.g. all objects of the same type,
maybe subclasses, but not anything completely different like numbers and user-defined objects.
If this is not the case, how can you possibly compare and sort them? Which sense it will make?
Maybe this constraint will relaxed in the future, but for the first version presented here, it holds.

In the **internal** comparison mode, the caller specifies how the key is made, and HashLedger
compare the key on his own. If the comparison procedure it's just a simple variation of `memcmp`,
you can just describe the key in terms of offset from the beginning (of course `0` is a perfectly
valid offset) and length and let the HashLedger do the boring work.
This is also a straightforward way to implement the key as a set of fields from the data itself,
if it holds. Instead of providing an external key, you can use one or more fields of your object
as a key (segment), much like one can do in SQL tables. That's doable in external comparison
mode too of course, but doing this ways is probably easier and quicker. And it was also fun to code.

In 2012 you no longer can drop something and just avoid the concurrency thing.
We now live in the {multi,many}core era after all.
I do not avoid the question. Straight to the point: so far the concurrency is coarse grained and
I'd like to collect some real world data before to dig down with finer locking.
I originally thought about one lock per index plus one lock per hash. Read-Write locks of course,
not mutexes. POSIX is kind enough to provide both.
However, I quickly realized this is looking for troubles because every write operation has to
update, and then lock, all the indexes anyway, so adding more locks in _this way_ just provides
more opportunity for deadlocks and no concourrency gains.
Bottom line: the first incarnation of HashLedger uses just one big Read-Write lock.


Make it work. Make it nice. Make it fast. Make it reusable. No wait, the last should be the second. Really.
-----------------------------------------------------------------------------------------------------------

Because of course in order to be reusable (and I mean _properly_ reusable) of course the code has to be
nice enough. Hopefully.

HashLedger is implemented as library in order to minimize the coupling and maximize the reusability.
Both shared and static libraries are made avalaible. All the needed dependencies are provided and bundled
in the package, even though bundling libraries is a controversial (at best) idea.

However, the issue of the embeddability is not as trivial as one can think at first glance.
First and foremost, I'm not sold about the convenience of bundling a new library just for one data structure,
as HashLedger is. Personally, I'll not do that unless that data structure will be critical for my code
(e.g. for the hashledger daemon, see below). So, as first step, HashLedger should be trivially
embeddable in a foreign codebase, and for that I mean cut 'n' pasted.

I know that's controversial at best and my former self will definitely not appreciate that.
But in the last years I got my share of real-world development and I know that things like that **happen**.
I'm not going to do anything to discourage that, and quite the opposite, allowing such a kind of integration
it's a good exercise for minimize the coupling and the dependencies of the code.

For a similar reason, and to foster the spread of the code, the license of HashLedger is very permissive.
Some (sub)components are licensed under a [3-clause BSD license](http://opensource.org/licenses/BSD-3-Clause),
the others under a [ZLIB-like license](http://opensource.org/licenses/Zlib).

If I, as author and developer, have to take a stance, my advice about the embeddability of external 
dependencies can be summarized as:

* If the dependency is widespread (e.g. included in the major unix-like variants), **DO NOT** bundle it.
* If the dependency is widespread, but if the API is not stable enough and/or the project depends on
  a very recent version, **DO NOT** bundle it, but write clear and loud on the documentation the
  requirements. Build the generic binary packages, if any, against such versions (...of course!)
* If the dependency is uncommon/unknown/not packaged, **bundle it**. BUT bundle in a well defined
  subtree. Do the bare minimum set of changes to the source code, ideally none, and identify
  them clearly. Report upstream if it's the case (think twice, the answer is _yes_ more often than you
  think. Moreover, the maintainer can always reject them. Just send the patch).
* If you bundle a package, try to avoid recursive builds most importantly try as hard as you can
  to avoid the build-within-build ugliness. Sometime, for the sake of practicability, it is unavoidable.
  **But sometimes it is not!** At least give it a shot!
* If you bundle a package, the two most important drivers for that move are (or should be) 
  practicality and predictability. But now you've bite the bullet, take fully advantage of the situation
  **and modify the upstream bundled code if it's needed! Do not add kludges in the calling code, fix
  the underlying package!** You have to admit, at very least to yourself, that **bundling is forking**,
  so take advantage of the situation. And of course **report the patches upstream!**
* Get ready as unbundle as soon as you can (you don't really to maintain foreign code!).
* Be responsive backporting fixes and security updates!


And that's what I'd do in the case of embedding/depending on HashLedger.


How about actually using it?
----------------------------

Having all said and done, every project, even the cooler and sexier one, need at least an application
to pass the stage of idea and/or sketch code and become real (that's the reason why I got my fair share
of ideas in the limbo, but that's another story).
HashLedger needs such an application too. With _application_ I mean an use case, some useful client code.
Of course HashLedger needs unit-tests too, as well the underlying hash and skiplists too.

So, as I said in the introduction, here it goes the idea which conglomerates a few other cool ideas
I collected in the past.
Enter `ledgerd`, a poor-man `memcached` look-a-like featuring HashLedger as workhorse data structure.
Of course `ledgerd` is not intended as real-world memcached replacement (but if it works like that for you
awesome! drop me a note, I'm curious!), but just a live showcase and application-test for HashLedger.

`ledgerd` will have the following features

* high-performance event-loop based daemon.
* [ubjson](http://ubjson.org/) as data interchange format.
* [memcached](http://memcached.org/)(-like) as interconnection protocol.

For memcache-like protocol I mean that while `ledgerd` will of course speak the vanilla memcached protocol, some
features will be added in order to exploit the HashLedger range queries.
But this also means that any memcache compliant client will be able to speak with `ledgerd` with zero effort.

Adding persistency and thus becoming a [memcachedb](http://memcachedb.org/) look-a-like it's out of the scope
of the project, at very least until my work on [SkipDB](http://mojaves.github.com/blog/2012/04/08/skipdb-chronicles-chapter-0-rationale/)
is completed. So, `ledgerd` will keep all the dataset in RAM. Then suddenly using ubjson instead of the plain
json make more sense :)


Y U DON'T WRITE MORE PYTHON?!?!?!
---------------------------------

(go [here](http://knowyourmeme.com/memes/y-u-no-guy) if you don't catch the reference in the heading)

Just after I had the `ledgerd` idea, I started dusting off my copy of [libevent](http://libevent.org/).
While wondering if it was less effort to learn the differences between 1.4 and 2.0 or switch to
[libev](http://software.schmorp.de/pkg/libev.html), two problems arised.
First, my [ubjson C library](https://bitbucket.org/france/ubjsonlib) is still stuck on design phase.
`ubjsonlib` is a fun project, so I'm still in search of a decent API. That's a stopper because
every other major component is ready and avalaible.

Second and most important: it's clear from the step zero that `ledgerd` is not intended as production
caching replacement (but again, if does that for someone, good for him! Let me know!).
So why do not take this chance to dust off my python and start a fresh and cool project?
In python every major component is avalaible, including the [simpleubjson](http://code.google.com/p/simpleubjson/)
package for dealing with ubjson.

Moreover, doing like that I got the chance to learn something about `ctypes` and about the network
programming in python and step out a bit from my comfort zone.
They say the performance penalty for using an high level language in network servers is not
too heavy, and that's another thing that makes me lean through python.

In the future, if HashLedger gains enough momentum, I'd like to add more `ledgerd` implementation
in order to compare the performance (and of course having fun writing them and learning something new)
in plain C (once `ubjsonlib` is ready) and using [nodejs](http://nodejs.org).


Python + gevent + ubjson + HashLedger = awesome homemade braindead memcache look-a-like
----------------------------------------------------------------------------------------

Being written - keep hitting F5/CTRL-R!


So far, so good. But talk is cheap. Now show the code.
------------------------------------------------------

[Here you go, on bitbucket](https://bitbucket.org/france/hashledger).
There isn't a terribly compelling reason to put it on bitbucket instead
of github, except the fact that I STILL like hg more than git. Except there maybe me more pythonistas
on bitbucket and more rubysts on github. The workhorse is the C part, but that's a tie.

And all of this reminds me the fact the octopress bitbucket plugin is still broken.
Looks like I'd really need to debug it.

PS: If someone is wondering what the heck "HashLedger" means or come from.
It's a (bad) pun from the [Honey Badger](http://www.youtube.com/watch?v=c81bcjyfn6U), actually. Yup.

# EOF
