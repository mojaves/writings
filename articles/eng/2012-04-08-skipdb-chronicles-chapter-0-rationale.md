Title: SkipDB Chronicles Chapter #0 - Rationale
Date: 2012-04-08 09:20
Tags: english, Clang, projects, dbmlike, skipdbchronicles
Category: projects
Slug: skipdb-chronicles-chapter-0-rationale
Author: Francesco Romani
Summary: thoughts about a dbm replacement based on skip lists instead of B-trees.


Intro
-----

Some time ago I stumbled into the (re)discovery of [log-based persistent
data structures](http://blog.notdot.net/2009/12/Damn-Cool-Algorithms-Log-structured-storage).
That, coupled with the raising NoSQL movement, ignited my interest in the dbm-like libraries;
for both my hobby and day-job project, to have an embeddable, fast key-value storage is
extremely useful. Unfortunately, for a series of circumstances, that interest failed to
produce anything valuable, until I stumbled into the Skip List and, most importantly,
in the [SkipDB project](https://github.com/stevedekorte/skipdb).
In this series of posts, I'll put down my notes about how SkipDB works, and how it can
be adapted (or, if unavoidable, rewritten) to be used as log-based storage.

<!-- more -->


What the Skip List are and what they can do for me?
---------------------------------------------------

A Skip List is a probabilistic data structure which provides `O(log n)` lookup, search
and delete efficiency, by a clever arrangement of the pointers in the metadata of the
elements of the list itself. Given the probabilistic nature of the data structure,
a robust random generator is required and, most important the `Big-O` bounds cannot
be guaranteed. However, the probability of a degneration in running time is tiny enough.

The Skip Lists are eloquently described in [a paper from the author](ftp://ftp.cs.umd.edu/pub/skipLists/skiplists.pdf).
The implementation, as the author (William Pugh) suggests, is short and direct,
spanning comfortably in [less than 300 lines of C](https://github.com/mojaves/cxkit/blob/master/kits/skiplistkit.c) 
(yet poorly tested, however).

The Skip Lists are the natural contenders of the balanced-trees family (AVL, Red-Black and so on).
So, why should you bother with those fancy Skip Lists when you already have an handy balanced
tree at your disposal?

You probably should'nt, actually. But, again echoing the paper, *if you don't have a well tested
balanced tree at your disposal*, then you should consider the Skip List most for their implementation
advantages.
Skip Lists are easy to build and easy to verify. On the other hand, building, debugging
and optimizing a balanced tree is not the easiest thing on the earth.
If you really care about memory consumption (or if you're forced to), it must be noted
that Skip Lists *can* be made quite memory efficient, weighting even less than a careful made
balanced tree.


SkipDB: a gem hidden in the lairs of GitHub.
--------------------------------------------

While tinkering with the Skip Lists and putting them to the test for a future use in my projects
(with very encouraging results,  if anyone wonders), some trigger rang in the back of my head and
after a quick search the SkipDB project popped out (both from GitHub and from my memory).

SkipDB is the brainchild of Steve Dekorte, which already released goodies like the
[io programming language](http://www.iolanguage.com/), the [BaseKit library](https://github.com/stevedekorte/basekit)
and a bunch of other you can find on [Steve's github page](https://github.com/stevedekorte).

SkipDB is interesting for an handful of good reasons: first and foremost, it uses the Skip List
data structure instead of the classical B-Tree. Second but important, the codebase is small
and easy enough to follow. I personally find the code of SkipDB well structured and quite instructive.
Talking about readability, to be honest, [google's LevelDB](http://code.google.com/p/leveldb/)
is quite a pleasent read too, but unfortunately it is not based on Skip Lists. :)

On the other hand, for a bunch of usually good (or at least unavoidable) reasons, most of the DBM like
libraries are quite convoluted and hard to understand.
That's the price to pay to be battle-tested and fixed, and we all know that.
But that doesn't change the fact that easier and cleaner libraries are just better for learning. :)


Chronicler Postil
-----------------

This post is part of the "SkipDB Chronicles". Lookup the `skipdbchronicles` tag for more chapters.


#EOF

