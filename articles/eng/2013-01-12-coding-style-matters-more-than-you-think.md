Title: Coding style matters more than you think.
Date: 2013-01-12 16:03
Tags: english, programming, essay, craftmanship
Category: essays
Slug: coding-style-matters-more-than-you-think
Author: Francesco Romani
Summary: the lack of coding standards will backfire, and it will do in subtle ways.


Intro
-----

So I have had this question rumbling in the back of my head for a while.

I am a passionate opensource developer, but it also happens I write software
for a living, so my experience is not limited to hobbystic, community-based
software development. Not at all.
I must say, I naturally lend toward an open, hackish culture. It just works better.
It just produces better quality software. It is also way more fun.
All the above, hands down.
To summarize, borrowing wise words from elsewhere:
"I can change what I think, but I cannot change what I see".

Having such a background, I naturally care *also* (not exclusively, of course)
for code quality, coding style, [code readability](http://eli.thegreenplace.net/2013/01/05/understanding-your-own-code/),
mantaining the code base in good shape.
I read and appreciated
[the practice of programming](http://www.amazon.com/Practice-Programming-Addison-Wesley-Professional-Computing/dp/020161586X/ref=sr_1_1?ie=UTF8&qid=1358003203&sr=8-1&keywords=the+practice+of+programming),
[clean code](http://www.amazon.com/Practice-Programming-Addison-Wesley-Professional-Computing/dp/020161586X/ref=sr_1_1?ie=UTF8&qid=1358003203&sr=8-1&keywords=the+practice+of+programming)
et al, which
devotes entire chapters, if not the whole thing to explain how to take care
of those aspects of the our craft (friendly remainder: those books are also
bestsellers and quite frequently recomended reads. Bear this in mind while
you read the remainder
of this humble essay).

<!-- more -->

The Young, the Reckless, the Young and Reckless
-----------------------------------------------

Among the computer engineering students I met back at times, apparently
*very* few cared about those topics.
The vast majority was just busy enough figuring out how make the software
work. Once it worked, everyone was happy, and can happily move to the next
task, or class.
We were young, many had never programmed anything before.
So, I thought: we are short on time and learning about our craft.
Of course having code that works is the first and most important step,
but eventually we all will realize it is just *the very first* step.
Let's trust in time and in the learning experience.
Time proven me mostly wrong, but that's a different story.

Then I started hanging around in the opensource world.
Reading the code, lurking the mailing lists.
My ideas just grew stronger. I had the evidence that those ideas are applied
in the wild. Just any established, even remotely succesful opensource project
with a number of contributors greater than one has

- coding rules/conventions/style. Written down and enforced.
- regularly rewrites code if it is not conformant.
- rejects not conformant code.
- sometimes has automatic tools to make the code conformant.

The rationale behind those choices it's so obvious that'll make your brain
hurt (and yet someone doesn't get it. However.)

On the other hand, I learned the hard way there is no, let's say,
_universal consensus_ in the industry about the importance of those topics.
They aren't exactly always in the top ten priorities on some teams.
There are reasons for that. That's not the point.
My point is different. The question that was puzzling me was simple:
why this does'nt bite back?
Why this different choice of priorities does not backfire?

Truth is: it does, but in not-obvious ways.

Borrowing from the future
-------------------------

Let's say this straight, and let's say this now:
The lack of coding standards is a form of technical debt.
There is no escape from this simple truth. Unless the coders are extremely skilled,
diligent and fluent, some inconsistency will slip into the codebase.
Some quick fixes and rush patches will introduce even more. And soon, you're
going down full speed on the slippery slope. The codebase becomes a messy patchwork
of different styles. Moreover, the carelessness is persistent, contagious
and, bottom line, [just depressing](http://en.wikipedia.org/wiki/Broken_windows_theory).

But it's really just simple as that? It's just lazyness, lack of discipline?
Or there can be something else, more subtler, on a more personal level.


Out of sight, Out of mind
-------------------------

After some thought exercises and some right reads, it came obvious
(of course, once you figured out, it's always obvious):
the stronger the [code ownership](http://martinfowler.com/bliki/CodeOwnership.html),
the lesser the lack of care about coding standards is a problem.
So, the problem exists (e.g. long turnaround time for maintainer changes.
Yes, this is also sometimes called `job security').
If the code ownership is very strong, every developer sees just the modules
it has to maintain. Strong barriers are in place.
Noone is allowed to change someone else's code, so what's the point in
having common standards and code readability?
As long as the designated maintainer is comfortable, everybody's fine.
Easy enough.

And I think that's actually a fine point, if strong code ownership is the
*right* and *desired* approach to a given software project.

But if this approach is not the most effective one, then the problems arising
from the lack of coding standards are dwarfed by the one given by a such
strong ownership.
And in the other hand, the lack of such standards prevents any attempt to move
towards a (more) collective code ownership.
This, and the very same culture that allowed and grew the strong code ownership
approach.


You are not your code
---------------------

This is really the other side of the coin. Maybe someone is designated as
maintainer of a software module. Or maybe someone _do not really want_ anyone
else "messing" with its code.

I'm not talking about rejecting sub-quality contributions or patches.
I'm not talking about maintaining the integrity of the architecture of a system.
(more on this topic later).
Those are really fine points and good practices. That's not what I'm talking about.

I'm talking about rejecting the whole concept than _someone else will
routinely change your code_. Is like strong code ownership, but the other way around.
Someone is not "designated". Someone does not want to let its code loose.
Let's call this *code jealousy*.

In the community-based, open source world, there is a *very strong* bias towards
[egoless programming](http://www.codinghorror.com/blog/2006/05/the-ten-commandments-of-egoless-programming.html),
of course for the very nature of the open and community based approach.
Bad code, or even good but suboptimal code, is quickly and sometimes harshly replaced,
because that's just the way it works.
You learn the hard way that you're not your code. You learn it fast. And you
cannot avoid that, you learn or you quit.

But from my viewpoint the good about this process greatly offset the bad, if you
are at least a decent programmer or if you want to become it.
The project on the whole grow stronger in an egoless world. The code gets better
and it gets better as quickly as possible, for the very reason the legacy and the burden
is minimized. And you can learn and improve a lot, if you are able and willing to keep up.


Looking for cohesion
--------------------

So, where is the harm in the *code jealousy*?
Except for the not-at-all-negligible factor of increased developer stress when reading
a codebase foreign in style (remember: sometimes the contingency _mandates_ to read
someone else code, maybe when hunting a bug. Maybe where hunting a production-blocking
bug. Are you really about to file a bug to your room-mate's software library in those cases?
Really? And for what reasons? For the right reasons?), there is an integrity problem.

A system is not just the sum of its parts. It is not, it _should_ not be an agglomerate
of components bolted together.
A system should have an architecture, and someone must oversee and protect its integrity.
It's the way succesfull project are developed.
It's the way right projects are developed.

Having an archipelago of different software modules each one with its own style and rules
is the exact opposite of this. It actively works against the architectural integrity of
a system.

Just like having a bunch of good players doesn't make a great team.
Teams win championships, not players alone.
Do you care about the players or about the team?


Outro
-----

My quest so far provided the answer above for my still-lasting Question.
The strong code ownership can be a deliberate choice. Can be a good or maybe optimal choice
depending on the context. But it has to be a choice, and not just "it's the way
we worked so far.". Because it has disadvantages too.
*Code jealousy* can be understandable. There can be reasons for this, but
I have still to find some good use for that. Any goodness from this comes as
collateral effect. And that's also one of the scenarios where the lack of standards
bites harder.


#### EOF

