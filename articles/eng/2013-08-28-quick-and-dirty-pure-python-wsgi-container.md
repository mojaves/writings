Title: Quick and dirty pure python WSGI container.
Date: 2013-08-28 21:46
Tags: english, programming, hacks, WSGI
Category: python
Slug: quick-and-dirty-pure-python-wsgi-container
Author: Francesco Romani
Summary: Sometimes you need a simple WSGI container which runs anywhere.


If you develop webapps in python, there is plenty of [WSGI containers](http://www.wsgi.org/en/latest/servers.html).
Unfortunately, they all have one thing in common: they expect a sane server environment, probably
any flavour of unix.
Truth to be told, this is a very reasonable assumption. Performant servers platforms are often
unix, and quite often they are *also* cheap and widespread enough. And they they are plenty of goodies.
Sometimes you are just out of luck and you need to deploy, at least temporary, at least for internal
usage, on less civilized platforms, like one coming from Seattle, WA.

In those case, something like this can be useful, at least as relief, or as a shoplist or
as kickstart for further development.

This is [grhino](https://gist.github.com/mojaves/6370356), a pretty simple WSGI container leveraging only pure python
modules, like the well known [cherrypy](http://www.cherrypy.org/).
It can run any WSGI application, and it is portable as python, as it is very simple and depends only on pure packages.

It is *way* less awesome than practically any other WSGI container around but, hey, it is available
here and now. Maybe can be useful for someone.

<script src="https://gist.github.com/mojaves/6370356.js">
</script>
