Title: Optimizing for the modern CPU caches.
Date: 2013-05-05 10:21
Tags: english, programming, Clang, tools, optimization
Category: essays
Slug: optimizing-for-the-modern-CPU-caches
Author: Francesco Romani
Summary: Just reordering the fields of your structs can give you speedups.


Intro
-----

So, you need some performance. And interestingly enough, you need single-core
performance. You cannot throw more cores at your application for whatever
reason (maybe you hit the wall of the [Amdhal law](http://en.wikipedia.org/wiki/Amdahl%27s_law)?)
In order to go really fast nowadays you must exploit the memory hierarchy,
most notably the caches of your CPU.
This concept is already well known from a while, since the historical paper
[What every programmer must know about memory](http://ftp.linux.org.ua/pub/docs/developer/general/cpumemory.pdf).
In this article, I will present a real-world cache optimization attempt,
step by step, along with details, the tools used and results obtained.

TL;DR roughly 7% speedup by reordering fields in an handful of structs and by dropping some code.

<!-- more -->

Delimiting the area
-------------------

Recently I am spending most of my hacking time on [Eon3D](http://github.com/mojaves/Eon3D).
Eon3D is a simple 3D software renderer written in C89, with some minor C99-isms.
The platform: GCC 4.7.2 as compiler, running on debian linux running the last 3.8.x kernel
on amd64 using cmake as build system and bash 4.2.x as shell. The iron is an (old) Intel
core i7 930 with 6 gigs of RAM. All the build will be compiled with `-O2`.

The tool used to inspect the memory layout is [pahole](http://lwn.net/Articles/206805/)
which you can hopefully find packaged pn your favorite distro in the `dwarves` package.

The Eon3D project is divided in modules. The core module is the renderer itself, which
translates a scene described by API calls in a RGBA raster (bitmap) image. Since the 
heavy lifting happens here, we will only analyze the core renderer.
To verify the impact of the changes, we will use the aptly called `benchmark` program
in the Eon3D packages. The testcase is to load a model described in the PLY format and
to render it for a fixed amount of frames (ten thousand for this run).
Execution time is the value being benchmarked, and the measuring is done using the
plain `time -p` from the GNU time package.


The Baseline
------------

Let's consider Eon3D revision `4b8665be324e7e141e470a607750b176aeb20a23`.
The starting point is the following, considering an average of 5 runs with powersaving disabled
(aka `performance` CPU governor).

`10000 frames in 55.07 seconds: 179.53 FPS`

Let's now esamine the core library with `pahole`. We will focus only on the structs
used in the inner rendering loop, which are the following:

    
    /* LISTING #1 */
    struct _EON_Face {
    	EON_Vertex *               Vertices[3];          /*     0    24 */
    	EON_Float                  nx;                   /*    24     4 */
    	EON_Float                  ny;                   /*    28     4 */
    	EON_Float                  nz;                   /*    32     4 */
    
    	/* XXX 4 bytes hole, try to pack */
    
    	EON_Mat *                  Material;             /*    40     8 */
    	EON_ScrPoint               Scr[3];               /*    48    36 */
    	/* --- cacheline 1 boundary (64 bytes) was 20 bytes ago --- */
    	EON_sInt32                 MappingU[3];          /*    84    12 */
    	EON_sInt32                 MappingV[3];          /*    96    12 */
    	EON_sInt32                 eMappingU[3];         /*   108    12 */
    	EON_sInt32                 eMappingV[3];         /*   120    12 */
    	/* --- cacheline 2 boundary (128 bytes) was 4 bytes ago --- */
    	EON_Float                  fShade;               /*   132     4 */
    	EON_Float                  sLighting;            /*   136     4 */
    	EON_Float                  Shades[3];            /*   140    12 */
    	EON_Float                  vsLighting[3];        /*   152    12 */
    
    	/* size: 168, cachelines: 3, members: 14 */
    	/* sum members: 160, holes: 1, sum holes: 4 */
    	/* padding: 4 */
    	/* last cacheline: 40 bytes */
    };
    struct _EON_Mat {
    	EON_Color                  Ambient;              /*     0     4 */
    	EON_Color                  Diffuse;              /*     4     4 */
    	EON_Color                  Specular;             /*     8     4 */
    	EON_uInt                   Shininess;            /*    12     4 */
    	EON_Float                  FadeDist;             /*    16     4 */
    	EON_uChar                  ShadeType;            /*    20     1 */
    	EON_uChar                  PerspectiveCorrect;   /*    21     1 */
    
    	/* XXX 2 bytes hole, try to pack */
    
    	EON_Texture *              Texture;              /*    24     8 */
    	EON_Texture *              Environment;          /*    32     8 */
    	EON_Float                  TexScaling;           /*    40     4 */
    	EON_Float                  EnvScaling;           /*    44     4 */
    	EON_Bool                   zBufferable;          /*    48     4 */
    	EON_uChar                  _st;                  /*    52     1 */
    	EON_uChar                  _ft;                  /*    53     1 */
    
    	/* XXX 2 bytes hole, try to pack */
    
    	void                       (*_PutFace)(EON_Cam *, EON_Face *, EON_Frame *); /*    56     8 */
    	/* --- cacheline 1 boundary (64 bytes) --- */
    
    	/* size: 64, cachelines: 1, members: 15 */
    	/* sum members: 60, holes: 2, sum holes: 4 */
    };
    struct _EON_Vertex {
    	EON_Float                  x;                    /*     0     4 */
    	EON_Float                  y;                    /*     4     4 */
    	EON_Float                  z;                    /*     8     4 */
    	EON_Float                  xformedx;             /*    12     4 */
    	EON_Float                  xformedy;             /*    16     4 */
    	EON_Float                  xformedz;             /*    20     4 */
    	EON_Float                  nx;                   /*    24     4 */
    	EON_Float                  ny;                   /*    28     4 */
    	EON_Float                  nz;                   /*    32     4 */
    	EON_Float                  xformednx;            /*    36     4 */
    	EON_Float                  xformedny;            /*    40     4 */
    	EON_Float                  xformednz;            /*    44     4 */
    
    	/* size: 48, cachelines: 1, members: 12 */
    	/* last cacheline: 48 bytes */
    };
    

The really hot structures are `EON_Face` and `EON_Vertex`, which are heavily
accessed in the rendering loop (see `EON_RenderObj` in `Eon3D/core/eon3d.c`
and its call graph for details).
`EON_Mat` is provided more for completness than for relevancy.


Step 1: Reusing padding for good
--------------------------------

When a sufficiently smart C compiler (do not listen the haters, GCC is smart enough)
finds a struct field which is not word-aligned, it automatically adds padding
in order to ensure aligned, thus optimal, access.
This, of course, unless the programmer explicitely asks otherwise: see the
packed attribute and friends.

The first trivial change is to reuse such padding space for good, by enlarging
the size of the field.
Quite often, there is very little benefit in having a field smaller than a word,
because the CPU will fetch anyway an entire word from caches (or memory).

So, pack more fields in a single, aligned, larger one (e.g. bitmasks),or just waste more space.
The culprit is: you cannot save space anyway *in this way*, so consciously
waste and live happily. Or try harder to save space :).

In the baseline listing #1 above, we can see quite a few spot to reuse the padding:
For example, in the `EON_Mat` declaration, we can happily replace `EON_Char` with
`EON_uInt16` and reuse the padding space.

Let's examine in more detail a couple of short examples.

We have:

    
    struct _EON_Light {
    	EON_uChar                  Type;                 /*     0     1 */
    
    	/* XXX 3 bytes hole, try to pack */
    
    	EON_3DPoint                Pos;                  /*     4    12 */
    	EON_Float                  Intensity;            /*    16     4 */
    	EON_Float                  HalfDistSquared;      /*    20     4 */
    
    	/* size: 24, cachelines: 1, members: 4 */
    	/* sum members: 21, holes: 1, sum holes: 3 */
    	/* last cacheline: 24 bytes */
    };
     

Which becomes

    
    struct _EON_Light {
    	EON_uInt                   Type;                /*      0      4 */
    	EON_3DPoint                Pos;                 /*      4     12 */
    	EON_Float                  Intensity;           /*     16      4 */
    	EON_Float                  HalfDistSquared;	/*     20      4 */
    };
    

The size of the struct is not changed, neither did the cacheline friendliness (more on that later),
but we can now access the padding space which will be introduced by the compiler anyway. So, better
to use it explicitely.

Another interesting example is

    
    struct _EON_FaceInfo {
    	EON_Float                  zd;                   /*     0     4 */
    
    	/* XXX 4 bytes hole, try to pack */
    
    	EON_Face *                 face;                 /*     8     8 */
    
    	/* size: 16, cachelines: 1, members: 2 */
    	/* sum members: 12, holes: 1, sum holes: 4 */
    	/* last cacheline: 16 bytes */
    };
    

Which can be fixed, in the sense of dissolving the hole, just by swapping the
order of the fields. If we want to be extra careful, we can also consider that it's a
private helper struct, so the ABI stability does not matter at all here.

The improved struct is

        
    struct _EON_FaceInfo {
    	EON_Face *                 face;                 /*     0      8 */
    	EON_Float                  zd;                   /*     8      4 */
    };
    

Simple and easy, and hole filled.

After a few structs are been fixed, but just by filling the holes and claiming the
padding space, it is time to recompile and benchmark. As expected, the gains, if any
are negligible:

`10000 frames in 54.95 seconds: 181.99 FPS`

Time to get more serious.


Step 2: Facing the problems
---------------------------

So far nothing substantial has changed. We just claimed some space, but the basic layout,
thus the cache efficiency, is the same. We still are on the square one.

The two most heavily used structs in the hot rendering path are `EON_Vertex` and `EON_Face`.
In memory, they are layed down in two big arrays. This is already a good move performance-wise
since it allows to leverage the space locality and the prefetching capabilities of the CPUs.

Remember: leveraging cache locality and prefetching delivers most of the performance, so
it can be more fructuous to do many cache friendly, packed loops instead of few (even possibly
just one) but with scatthered access, destroying the cache coherencies.

Given the fact the structs we're examining are layed down in arrays, the optimal
size of such structs is an *exact* multiple of a cache line.

>
> Digression: what's a cacheline?
> Let's dig wikipedia for a quick summary:
> 
> [...] Data is transferred between memory and cache in blocks of fixed size, called cache lines.
> [...]
>
> Then, a cache line is the minimum amount of data that can be transferred. This justifies
> the asserion above about the need of having an optimal size multiple of a cache line
>

I got a cache line of 64 bytes, which is pretty much the standard in modern (>= 2002) x86/x64
world.

`EON_Vertex weights 48 bytes`. Would 64 bytes aka 1 cacheline be better?
Adding explicit padding did not gave any noticeable benefit (benchmark omitted).

Of course, `EON_Vertex` could be reorganized to fit in exactly 32 bytes: the `xformedn*` fields
can be calculated on demand wherever needed) but this move will require quite a few changes
which are out of scope now.
Let's leave `EON_Vertex` alone. Focus on `EON_Face` which offers more low-hanging fruits.

Here it is again.

    
    /* LISTING #2 */
    struct _EON_Face {
    	EON_Vertex *               Vertices[3];          /*     0    24 */
    	EON_Float                  nx;                   /*    24     4 */
    	EON_Float                  ny;                   /*    28     4 */
    	EON_Float                  nz;                   /*    32     4 */
    
    	/* XXX 4 bytes hole, try to pack */
    
    	EON_Mat *                  Material;             /*    40     8 */
    	EON_ScrPoint               Scr[3];               /*    48    36 */
    	/* --- cacheline 1 boundary (64 bytes) was 20 bytes ago --- MARK #1 */
    	EON_sInt32                 MappingU[3];          /*    84    12 */
    	EON_sInt32                 MappingV[3];          /*    96    12 */
    	EON_sInt32                 eMappingU[3];         /*   108    12 */
    	EON_sInt32                 eMappingV[3];         /*   120    12 */
    	/* --- cacheline 2 boundary (128 bytes) was 4 bytes ago --- MARK #2 */
    	EON_Float                  fShade;               /*   132     4 */
    	EON_Float                  sLighting;            /*   136     4 */
    	EON_Float                  Shades[3];            /*   140    12 */
    	EON_Float                  vsLighting[3];        /*   152    12 */
    
    	/* size: 168, cachelines: 3, members: 14 */
    	/* sum members: 160, holes: 1, sum holes: 4 */
    	/* padding: 4 */
    	/* last cacheline: 40 bytes */
    };
    

The basic `EON_Face` weights 168 bytes. It needs three cachelines, the last of which is not
completely full. Moreover, both the cacheline boundaries (see MARK #1 and MARK #2 above)
are crossed by some fields
(e.g. we have a single, composite, field which span two cachelines. An access to such field
will require a fetch more).

To summarize, to fix `EON_Face` we need:

* To reorganize the fields to avoid cacheline boundaries. Each field should weight an exact
  multiple of a cacheline, or, even better, weight less than one.
* To regroup the fields in order to exploit cache locality. Fields which are used together
  should belong to the same cacheline.
* If possible, to shave 40 bytes to make the struct occupy exactly two cachelines.


Interlude #1: Following the current line of thought, It is obvious we don't really need so much duplication of information, also know as redundancy (pun intended)
------------------------------------------------------------------------------------------------------------------------------------------------------------------

In order to implement the most basic shading technique, aka Flat Shading, we need two fields
to store the amount of light which hits face (aka the triangle) and the actual computed value of
the shade. That's the purpose of the `fShade` and `sLighining` fields.
But in order to implement any more refined shading technique, we will need the same
information for each vertex. Moreover, the usage of such group of fields is mutually exclusive,
given the trivial truth that a given face can be shaded using just one algorithm.

First easy move: drop the explicit fields and use the zero-th vertex shading information for
Flat Shading. Boom! 4+4 bytes trimmed. Has this gave any performance benefit? Hardly.

`10000 frames in 54.34 seconds: 184.03 FPS`


Interlude #2: A Necessary Evil
------------------------------

The Eon3D project is built on the legacy of the [Plush3D renderer](https://github.com/OrangeTide/plush).
Plush3D is project made in the early 90s, and it is quite featured considering the standards of the time
and for being a software-only renderer. Plush3D is good code. It is a well written, well enginereed and
quite fast library, but it's also roughly 20 years old, and some features inherited from it have little
value for Eon3D, at least in the current incarnation.

One of which dubtiouos feature is Environment Mapping. Supporting it requires two fields in `EON_Face`,
weighting 24 bytes in total. If we can trim those two we'll get really close to the 40 bytes
shrinking goal.

For this specific case, the optimization endeavour provides the final bit of motivation to shred the
Environment Mapping feature, thus the related fields from `EON_Face`.
This is not an easy step, because doing so allows to go fast and loose on the easy route.
In more constrained (read: real world) scenario, the story would have been much different.
However, for the sake of the experiment, let's go ahead.

The remaining 4 bytes left are kindly provided by the hole in te struct detected by pahole, as shown
in the listing(s) above.
Now we have found 40 bytes and we can focus on reordering the fields to improve the cacheline efficiency.


Step 3: In just two Cache Lines
-------------------------------

The optimal memory layout for data depends on the actual data and its usage.
However, we can distill some general guidelines to use as starting point.

* The data should never cross a cacheline boundary.
* The data should occupy an exact multiply, or submultiply, of a cacheline.
* The data should be laid down as much sequentially as is possible, in order to leverage
  hardware prefetching.
* However, the data should also leverage cache locality, so data used together should
  be placed in near locations.

This is not rocket science, however in practice to fullfill at least all those constraint
can be tricky.

It is quite possible that the optimal memory layout actually conflicts with a clean
representation and separation of data from a maintainbility standpoint.

To do a meaningful reorganization we need a few more details about Eon3D works.
Being a software renderer, the library performs two basics tasks (sorry graphics
experts for the gross oversimplification):

* geometry transformation: figure out how to put on a 2D surface (if needed at all) a point
  declared being in a 3D environment. Same for lines, triangles and any geometric shape.
* rasterization: figure out the right color for every such point projected in a 2D surface.

The fields in `EON_Face` are used for those two tasks, as follows:

    /* LISTING #3 */
    struct _EON_Face {
    	EON_Vertex *               Vertices[3];          /* geometry */
    	EON_Float                  nx;                   /* geometry */
    	EON_Float                  ny;                   /* geometry */
    	EON_Float                  nz;                   /* geometry */
    	EON_Mat *                  Material;             /* raster   */
    	EON_ScrPoint               Scr[3];               /* geometry, raster */
    	EON_sInt32                 MappingU[3];          /* raster */
    	EON_sInt32                 MappingV[3];          /* raster */
    	EON_sInt32                 eMappingU[3];         /* raster */
    	EON_sInt32                 eMappingV[3];         /* raster */
    	EON_Float                  Shades[3];            /* raster */
    	EON_Float                  vsLighting[3];        /* raster */
    };
    

The tricky point is the `Scr` field, which is used both as output for the geometry step and input
for the reaster step and, moreover, is the biggest one.

After a few attempts, the best layout emerged so far is the following:

    
    struct _EON_Face {
    	EON_Vertex *               Vertices[3];          /*     0    24 */
    	EON_ScrPoint               Scr[3];               /*    24    36 */
    	EON_Float                  nx;                   /*    60     4 */
    	/* --- cacheline 1 boundary (64 bytes) --- */
    	EON_Float                  ny;                   /*    64     4 */
    	EON_Float                  nz;                   /*    68     4 */
    	EON_Mat *                  Material;             /*    72     8 */
    	EON_Float                  Shades[3];            /*    80    12 */
    	EON_Float                  vsLighting[3];        /*    92    12 */
    	EON_sInt32                 MappingU[3];          /*   104    12 */
    	EON_sInt32                 MappingV[3];          /*   116    12 */
    	/* --- cacheline 2 boundary (128 bytes) --- */
    
    	/* size: 128, cachelines: 2, members: 10 */
    };
    

Again, this is tricky. An obvious clean solution is not yet available.
While at first sight all the constraints outlined above are satisfied, there are
some points still open for critics:

* The `nx`, `ny`, `nz` fields are really a single EON_3DPoint in disguise,
  thus a single struct field. There still is a cacheline boundary crossing
  lurking just behind the surface.
* The `Scr` field is the biggest source of troubles. From a purely logical standpoint
  it should be placed at the end of the first cacheline. In practice, as long as
  it fits in just one cacheline, it can be placed anywhere.
* From the performance standpoint it would be wise to unpack it in order to reorganize
  the layout again.
* We really need to squeeze 8 more bytes in the first cacheline.

However, although with some stretching of the rules, we eventually got what we were
looking for. We managed to fit the performance critical `EON_Face` struct in exactly
two cachelines. Let's see if all of this was worth the journey:

`10000 frames in 51.39 seconds: 194.59 FPS`

We trimmed down roughly 4 seconds on a former total of 55, which is a significant result
for just rearranging some fields. A little more of a 7% performance boost, as promised.

Outro
-----

In this particular case we stretched the rules by shedding a feature, although not needed
and inherited as legacy, in order to optimize the layout. Stated this fact, the findings
and the results we've obtained are worth the effort

* Just by reorganizing the fields in the hot structs we achieved a 7% speedup.
  No algorithm changes at all! No compiler magic, no library upgrade.
* The `pahole` output gives the information you need to optimize for optimal cachelines
  usage.
* The way you organize your data in memory have a real impact on the performance.
* You *NEED* to exploit and use cleverly the CPU caches if you want to go faster.

If you want to get away just with one sentence from this article, yet again the bottom line is:
If you had formal CS education, be careful: the machines you're programming on are quite likely
different beasts with respect on which you're been thought.
We are not in the 90s anymore, do not program like you're still stuck on.

#### EOF

