Title: Pythonic powerful multimedia processing with pyrana.
Date: 2013-10-06 17:30
Tags: english, programming, projects, python, pyrana, ffmpeg
Category: pyrana
Slug: pythonic-powerful-multimedia-processing-pyrana
Author: Francesco Romani
Summary: Pyrana is pymedia rebooted and provides easy access to ffmpeg libraries.


Intro
-----

What is [pyrana](http://bitbucket.org/mojaves/pyrana)?
Pyrana is a pure-python package, [available on pypi](https://pypi.python.org/pypi?:action=display&name=pymedia2-pyrana),
which provides easy, pythonic and powerful handling of multimedia files.


- easy: pyrana does not submerge you with tons of options and details,
  but is filled with sane defaults. pyrana aims to be multimedia processing
  what [requests](http://docs.python-requests.org/en/latest/) is for http.

- pythonic: pyrana wants to play nice and work well with the other well-established
  relevant python packages: [Pillow](https://pypi.python.org/pypi/Pillow),
  [pygame](http://pygame.org), [PySDL2](http://pysdl2.readthedocs.org/en/latest/).
  [numpy](http://www.numpy.org/) compatibility is coming soon.

- powerful: pyrana provides an independent API, but is built on the great foundations
  provided by the [powerful FFMpeg libraries](http://ffmpeg.org).


pyrana is a modern, pure python package which is developed for python 3 (but python 2.7 compatibility
is coming soon!) which takes great advantage of [CFFI](http://cffi.readthedocs.org/en/release-0.7/),
so the compatibility with [pypy](http://pypy.org) is just one step away.

pyrana offers a minimum 100% unit-test and documentation coverage, and put great emphasis on small, yet
complete and workable examples.
Last but not least, pyrana is released under the very liberal ZLIB license. 

Intrigued? just keep on reading. :)

<!-- more -->


What will you find in the first release
---------------------------------------

The first release will be the 0.2.0, which will be alpha, and not feature complete.

Pyrana just still lacks real-work, battle testing, so it is marked as alpha.
But we took testing pretty seriously. Pyrana features an extensive battery of more than 140 unit tests,
still growing, and, according to `sloccount`, more lines of testing (modulo the forthcoming refactoring)
than actual code, just like in the jokes :) (1211 vs 1208 lines, for the curious.)

The 0.2.0 release laid the foundation of the package, providing full demuxing and audio/video decoding.
Encoding and Muxing support are of course coming soon in future releases.


A simple example
----------------

I will now introduce a simple yet complete, runnable example. I will go quite fast and assume the reader
has at least familiarity with a few key multimedia concepts like Pixel Formats, data planes, frames,
stream multiplexing and so forth.

Without further ado, let's take a look to a simple yet meaningful example featuring pyrana and pygame!

    
    import sys
    import pygame
    import pyrana
    from pyrana.formats import MediaType  # shortcut

    
    def play_file(fname):
        with open(fname, "rb") as src:
            dmx = pyrana.formats.Demuxer(src)
            sid = pyrana.formats.find_stream(dmx.streams,
                                             0,
                                             MediaType.AVMEDIA_TYPE_VIDEO)
            vstream = dmx.streams[sid]
            width = vstream["width"]
            height = vstream["height"]

            pygame.display.set_mode((width, height))
            ovl = pygame.Overlay(pygame.YV12_OVERLAY, (width, height))
            ovl.set_location(0, 0, width, height)
    
            vdec = dmx.open_decoder(sid)
    
            while True:
                frame = vdec.decode(dmx.stream(sid))
                img = frame.image()
                ovl.display((img.plane(0), img.plane(1), img.plane(2)))
    
    
    if __name__ == "__main__":
        if len(sys.argv) == 2:
            pygame.init()
            pyrana.setup()
    
            play_file(sys.argv[1])
        else:
            sys.stderr.write("usage: %s videofile\n" % sys.argv[0])
            sys.exit(1)
    

This is actually a stripped-down version of a [real pyrana example](https://bitbucket.org/mojaves/pyrana/src/bc63ddb0d2a6465385ff580d88ad0b2b3a878e03/examples/11_compat_video_pygame.py?at=default).
Let's see what we got in greater detail.

Important things first. Much like pygame, before the client code uses anything from pyrana, you need to initialize
the package by calling `pyrana.setup()`. It is safe, although NOT recomended, to call multiple times the initialization
function. Once everything is set up, we can now concentrate on the bulk of the example, the `play_file` function.

First and foremost, to decode data, you need a source of data. Pyrana accepts every already-opened, externally-managed
(pyrana NEVER takes ownership of instances gave to it except for the obvious internal references, which are hard refs, not
weakrefs) file-like. So

    
    with open(fname, "rb") as src:
        dmx = pyrana.formats.Demuxer(src)
    

Now the Demuxer is ready to roll. Demuxer untangle the media files and provides to the client access to the logical
streams embedded in them, so you can extract all the audio or video packets.
Demuxers *discard* the packets which belong to streams you do not have requested, so if you need N separate streams,
you will need N separate, independent stacks of file-like and Demuxers.

Demuxers exports a python-firendly view of the metadata. You can navigate it using the standard ways, because it is
made of standard, plain python objects. The function `find_stream` will help you to find the stream identifier (`sid`)
of the N-th stream of the given kind. In the following lines, we look for the 0-th (so the first :)) video stream;
then extract the width and height properties by the stream informations, presented as dict(-like).

    
    sid = pyrana.formats.find_stream(dmx.streams,
                                     0,
                                     MediaType.AVMEDIA_TYPE_VIDEO)
    vstream = dmx.streams[sid]
    width = vstream["width"]
    height = vstream["height"]
    

The next lines just fire up an hardware overlay using pygame. Nothing new.

    
    pygame.display.set_mode((width, height))
    ovl = pygame.Overlay(pygame.YV12_OVERLAY, (width, height))
    ovl.set_location(0, 0, width, height)
    

Now things starts to get interesting. We ask the Demuxer instance to provide a suitable, ready to-go, Decoder
for the given stream-id.

    
    vdec = dmx.open_decoder(sid)
    

And finally, the real decode-show loop. It ends automatically because a Demuxer raises EOSError (much like
EOFError or StopIteration) when it can no longer extract a Packet from the requested stream.
Unsurprisingly, the `decode()` method of a Decoder takes one or more `Packets`, as provided by a Demuxer,
and returns back a decoded Frame of the appropriate kind (or an appropriate exception on error, of course).
`The `decode()` expects to actually consume a stream of data, so it requires to be passed a generator or
a list. More access methods can be implemented in future releaes.
By consuming the spurce, the decoder is (more) sure to see the encoded Packets the right number of times.

    while True:
        frame = vdec.decode(dmx.stream(sid))
        img = frame.image()
        ovl.display((img.plane(0), img.plane(1), img.plane(2)))
   
Then you need to extract (do not worry! is a cheap operation) the actual Image, aka the real data, from
the Frame. Once you get an Image instance, you can extract further the data planes, according to the 
stream Pixel Format, and fed them into the pygame Overlay instance for the actual display. 


Coming next...
--------------

This is just a teaser trailer of what pyrana is and what it can does. Future articles will
explore both the internals of the package, with the design and implementation concepts,
and the practical uses of the package. Stay tuned!

### EOF

