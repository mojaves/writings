Title: Automatic plugin parameter handling for frei0r
Date: 2012-09-16 14:27
Tags: english, Clang, frei0r, projects
Category: hacks
Slug: automatic-plugin-parameter-handling-for-frei0r
Author: Francesco Romani
Summary: in order to make things fancy and easy you do not necessarily need C++.


Intro
-----

The development of the [tycho plugin set](https://github.com/mojaves/tycho-plugins)
gave me the option to tinker with the frei0r API and to develop
a small set of extensions and helper code, following the lines
of the `frei0r_math.h` and `frei0r_colorspace.h` helpers.

The first I'm about to document is the param helper,
implemented in `frei0rx_param.h` header.
This helper provides the set_param and get_param API calls,
which are all quite similar in structure and mostly boilerplate
code.
This helper also provides simpler and automagic access to the
fields of the instance structures.

<!-- more -->


Client code change checklist
----------------------------

A plugin which want to use the param helper has to be changed as
follows:

1. include the `frei0rx_param.h` header
2. add an array of `f0rx_param_attr_t` to the instance structure,
   one per tunable parameter.
3. provide a param getter function. This function acts as bridge
   between the plugin and the helper code. It cannot be provided
   by the helper for reasons described below.
4. extend the construct frei0r API to bind the variables to be
   exported as parameters to `param_attr`s.
5. provide an optional post_set consistency check callback

All those steps, except the first one which is straightforward,
is described in detail in the following sections.


The param_attr structure
------------------------

The `f0rx_param_attr_t` structure is bridge between the helper
code and the plugin code. While it has to be defined in the
included header, it has to be considered fully opaque for the
plugin code.

the param_attr structure is manipulated in two ways:

1. it is bound to the real plugin variable to be set/get
2. it is (opaquely) provided using the getter function to the
   helper code.

Because the param_attr structure is the bridge, there is the need
to have one per tunable parameter, thus an array of 
`plugin_info.num_params` items.
This array can be placed anywhere in the plugin instance data
structure.


The param_attr getter function
------------------------------

The lack of placement constraint for the param_attr array
brings the need of an explicit getter function which always has
the same structure but needs to be implemented in every plugin
The structure of the getter function is quite always the same:

    
    static f0rx_param_attr_t *f0rx_get_param_attr(f0r_instance_t instance,
                                                  int param_index)
    {
        PLUGIN_INSTANCE* inst = (PLUGIN_INSTANCE*)instance;
        return &inst->attrs[param_index];
    }
    

Where PLUGIN_INSTANCE has to be substituted with the actual plugin
instance structure name.


Binding param_attrs to the plugin variable
------------------------------------------

In order to let the helper actually change the setting and give back
a meaningful value to the client code, the param_attrs must to
be bound to the real plugin variable.

This is done by using the binding function:

    
    void f0rx_bind_param_attr(f0r_instance_t instance,
                              int param_index,
                              void *field);
    

Using the function it is simple at is gets: the `field` pointer has
to be set to the address to the variable correspinding to the
`param_index`-th parameter. Everything else is automagically handled
by the helper code.


Using the consistency check callback
------------------------------------

It is possible that two or more paramters are actually mutually dependent
and thus some internal data structures of the plugin instances have to
be updated when any of those values changes.
Normally, this update is done in the `set_param` API call.

Given the fact that the param helper pulls away this API call from the hands
of the programmer, an optional callback is provided which is called
after every write of the parameters.

If no explicit callback is provided, the helper transparently insert a
NO-OPERATION callback for every parameter bound.

The prototype of the callback is

       
    void post_set_callback(f0r_instance_t instance, int param_index);
       

Where `param_index` is the index of the parameter just updated.
The callback code has thus the possibility of update any internal
data structure if needed.

In order to attach a post set callback to a parameter, the plugin code
can use the following function:

    
    void f0rx_hook_param_attr_post_set(f0r_instance_t instance,
                                       int param_index,
                                       void (*hook)(f0r_instance_t instance,
                                                    int param_index))
    

Where `param_index` is of course the index of the parameter to which
the callback has to be attached.
The client code can use the special value `F0RX_PARAM_ALL` to automatically
attach a callback to every param_attr.


Example
-------

XXX

EOF

