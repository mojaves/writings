Title: Il potere del contesto
Date: 2012-01-15 21:45
Tags: italian, programming, essay, craftmanship
Category: essays
Slug: the-power-of-the-context
Author: Francesco Romani
Summary: (Italian) how the context affects performances in programming.


Intro
-----

L'insoddisfazione verso qualcosa non nasce necessariamente da singoli elementi
piu' o meno nascosti, ma comunque discreti, identificabili e dotati di un perimetro.
A volte, ci sono tanti piccoli fattori nascosti che contribuiscono a creare un disagio
sotterraneo e nascosto, che non si riesce ad esprimere, ne, quanto e' peggio, a identificare.

L'argomento potrebbe essere interessante e generale, ma il mio scopo e' limitarmi
al mondo della programmazione e del codice.
Cosa rende piacevole lavorare su un progetto? Cosa, al contrario, lo rende pesante e fastidioso?

<!-- more -->

Per prima cosa, trova il problema.
----------------------------------

Affrontiamo per prima cosa la questione base. Questo presunto disagio nel lavorare ad
un dato progetto puo' essere un problema?

Se si lavora su base hobbystica, per divertimento, magari rilasciando codice gratis
su bitbucket o github, il disagio e' **IL PROBLEMA** principale, perche' naturalmente
mina l'**entusiasmo**. L'entusiasmo e' il motore fondamentale di questo tipo di sforzi,
che tiene vivo il progetto, che motiva i partecipanti. Lo sviluppo ulteriore del
progetto genera altro entusiasmo, che alimenta un circolo virtuoso.

Il disagio il nemico naturale dell'entusiasmo. Non l'unico, ma probabilmente il peggiore.
Il disagio erode l'entusiasmo. Ne e' un antagonista naturale. Il disagio mina quindi le stesse basi
che sostengono quello sforzo.
Dunque, com'era forse prevedibile, nel caso di progetti hobbystici il disagio e'
tollerabile solo per brevi periodi e in circostanze occasionali.

Se si lavora su base stipendiata, come professione, le cose naturalmente cambiano.
Non si gioca piu', si lavora per vivere. Per potersi permettere il pane e un tetto.
Il lavoro non deve piacerci per forza. Va fatto, tanto basta.
E' vero.
Tralasciando l'obiezione positivista che neppure il lavoro dovrebbe essere una condanna
(lo facciamo per la maggior parte della nostra vita, dopo tutto), il fatto e' che il disagio
crea complessita' accidentale.

Il disagio diventa materialmente un costo, che quindi vale la pena rilevare e ridurre.
Il disagio alimenta la procrastinazione. Chi non ha rimandato (o almeno e' stato tentato di farlo)
un lavoro su un progetto che non piace?
Il disagio distoglie la concentrazione. Chi non ha provato la sensazione di volerne uscire
piu' in fretta possibile, lavorando su un progetto che non ci piace? Questo si manifesta
in varie forme, come il licenziarsi, oppure, andando verso risoluzioni meno estreme,
l'uscire alle 18:00 spaccate, o anche solo fare una modifica piu' velocemente possibile
per passare ad altro prima possibile.
Viceversa, se il progetto e' confortevole, non abbiamo _automaticamente_ una parte del cervello
impegnata a pensare come uscirne prima possibile. Possiamo immergerci di piu' nella situazione
e lavorare in maniera piu' organica. In maniera migliore.


E' tutto intorno a te.
----------------------

Abbiamo ammesso che puo' esistere un disagio nel lavorare su certi progetti software.
Possiamo verificarlo nella realta', probabilmente lo abbiamo gia' sperimentato piu' volte.
Abbiamo visto che, eccezion fatta per lo studio e la sperimentazione, se si sviluppa questo
e' un problema, un problema rilevante.

Quali sono le cause di questo disagio?
Il fattore che mi interessa e' quello tecnico.

**Il codice intorno a te condiziona il tuo modo di lavorare.**
**Il codice intorno a te e' il tuo contesto.**

Non e' solo il paradigma di programmazione, o gli strumenti che si devono utilizzare,
o un insieme pattern che un dato framework puo' spingere, o richiedere, o suggerire.

E' la pura forma piu' esterna del codice che condiziona il modo di lavorare. E' la sua struttura estetica.
Anche la forma del codice contribuisce alla salute generale del progetto.


Macellando vacche sacre.
------------------------

Aldo Cortesi e' anche autore di un notevole lavoro sulla [visualizzazione degli algoritmi](http://corte.si/posts/code/visualisingsorting/index.html).
Ma qui e adesso interessa piuttosto quanto segue:

> Good programmers know that a lack of superficial code quality and consistency
> is the best indicator of deeper systemic problems in a project.

Questa [tesi](Aldo Cortesi http://corte.si/posts/code/reading-code.html), puo' suonare come eretica.
Puo' suonare eretica perche' pare assai arduo desumere da sciocchezze quali 
la coerenza dell'indentazione,
da inutilita' quali la consistenza dello stile, da quisquilie quali la scelta dei nomi, la qualita' generale di un progetto.
Come potrebbe essere possibile? Non sono forse queste qualita' superficiali? Non sarebbe come dire che le automobili
rosse sono _ipso facto_ piu' veloci delle altre?
Quel che conta, si pensa, e' l'architettura del progetto e, in ultima analisi, se questo riesce
a produrre dei risultati, ad essere utile per qualcuno.

O forse queste **_sono scuse comode e rapide per evitare il problema._**

Ed e' **esattamente** quello che sostengo. Cortesi ha ragione, e sta semplicemente riformulando un vecchio aforisma:

> Il codice dovrebbe essere scritto per essere letto da un essere umano,
> e solo incidentalmente per essere eseguito da un calcolatore.

Purtroppo non ricordo la citazione esatta, ne' l'autore.
Dunque, se il codice veicola le intenzioni del programmatore, non si puo' prescindere dal valutarne **la forma** nel novero
delle sue qualita'.
Potrebbe essere interessante indagare su quanto nel codice la forma sia in realta' sostanza, ma questo sara' eventualmente
tema da affrontare in altra sede.

Tornando al software, consegnare un progetto deve essere il primo obiettivo. Del resto, che senso ha lavorare se non per
conseguire un obiettivo?
Terminare, consegnare, arrivare ad un punto fermo, ad una milestone, dovrebbe essere la prima priorita', sia
banalmente una questione professionale, sia anche nel mondo hobbystico: [fatto e' meglio di perfetto](http://lifehacker.com/5864004/the-done-manifesto-lays-out-13-ground-rules-for-getting-to-done).

Ma "finito e consegnato" non e' il feticcio cui si puo' sacrificare tutto il resto. Se il mondo finisse alla consegna, si,
potrebbe esserlo, perche' cosi' facendo nessuno paghera' mai tutto il [debito tecnico](http://avdi.org/devblog/2011/08/15/the-coding-wasteland/)
eventualmente accumulato.

Se nessuno dovra' manutenere quel progetto in cui tutto e' stato trascurato pur di portarlo in porto, o se quel tapino sara'
qualcun'altro (ma sara' _sicuramente_ qualcun'altro?) 
possiamo goderci il nostro pasto gratis: il debito lo ripianera' quel qualcuno, o quel qualcuno ne morira' schiacciato.
In ogni caso, il problema non e' il nostro.

Ma c'e' una scappatoia per questo scenario apocalittico. Si puo' analizzare e scomporre il debito tecnico nei vari fattori,
fattori di cui la bellezza superficiale e' tipicamente considerato solo il minore, gli spiccioli di questo debito.
Il grosso del debito, o al contrario della bonta',
e' normalmente dato da qualita' misurabili del codice (aderenza alle specifiche, correttezza -carenza di bug-, performance, per esempio),
l'architettura di base, la scelta di framework e strumenti.

E' incontestabile che questi fattori influenzino in maniera decisiva la vita di un progetto, e le sue performance in senso lato.
E' innegabile che le scelte architetturali abbiano peso elevato.
E' anche innegabile che progetti con scelte architetturali ragionevoli, o non peggiori di tanti altri, collassino sotto il loro peso.

Sostengo la tesi di Cortesi. Sono convinto della sua veridicita'. Ma provo a fare l'avvocato del diavolo.
Rifugiamoci in questo pensiero confortante: l'architettura e l'infrastruttura sono le fondamenta
su cui ogni progetto si basa. Se tale fondamento e' solido, se tale appiglio e' saldo, il resto ha un peso trascurabile.
Certo, in fondo sarebbe carino avere codice ben scritto, ma dopo tutto l'importante e' che funzioni.
Dunque, forti dell'appiglio dato da una buona infrastruttura, sia come architettura software sia come scelta di strumenti di supporto,
possiamo persino ammettere per assurdo che la tesi di Cortesi sia valida, e provare a valutarne le implicazioni.
Il nostro appiglio e' solido, non cedera'.


Chi si ferma e' perduto (non avere tempo per i dettagli).
---------------------------------------------------------
  
Codice brutto e' spesso codice scritto di fretta. E' vero che la coerenza, lo stile, l'eleganza sono dettagli di rifinitura,
che quindi si fanno per ultimi e comunque hanno peso minore rispetto al risultato e alla correttezza, non necessariamente
in quest'ordine. Ma proprio perche' si fanno per ultimi, la loro mancanza e' un indicatore importante.
Se non c'e' mai, o raramente, il tempo di curare questi dettagli, vuol dire che il progetto e' in affanno. Muta troppo
velocemente. Sta continuando ad accumulare debito tecnico, invece di ripianarlo. Piu' debito si accumula, piu' e' difficile
ripagarlo. Ad un certo punto il default e' inevitabile.
Dunque, una carenza di bellezza superficiale indica problemi piu' gravi all'interno del progetto.


Effetto arlecchino (non avere occhio per i dettagli).
-----------------------------------------------------
 
Ma cosa succede se scrivere codice brutto e' l'unico modo in cui si sa lavorare? Se, in altre parole, anche avendone il tempo
non si ha proprio la mentalita' di scrivere codice bene? Organizzato, coerente, facile da seguire.
Rifugiandoci nel porto sicuro, si potrebbe (ri)dire: chi se ne frega, l'importante e' il risultato.
Vale nei due sensi, l'importante e' il risultato. Codice brutto e' codice piu difficile da manutenere, piu' costoso da manutenere,
con piu' probabilita' di introdurre nuovi bachi o regressioni quando si manutiene. Codice che crea disagio.
Codice che non riesce a mantenere una sua struttura e una sua integrita' architetturale, la cui entropia
inevitabilmente aumenta, ma per imperizia, non (solo?) per carenza di risorse.
Codice che accumula debito tecnico, stavolta ancora piu' serio.
Dunque, una carenza di bellezza superficiale indica problemi piu' gravi all'interno del progetto.


L'importante e' che funzioni (E' tollerato non avere cura per i dettagli).
--------------------------------------------------------------------------

Ma perche' dovrebbe essere permesso a chi lavora su un progetto di scrivere codice brutto? Scrivere codice molto bello,
cosi' come molto performante, o subito corretto, o che risolve problemi molto difficile, puo' effettivamente richiedere
un talento non comune. Ma scrivere codice ragionevolmente bello non e' piu' difficile che scrivere codice ragionevolmente
performante o corretto. Richiede di identificare il problema, documentarsi e disciplina. E' alla portata di grandissima
parte dei programmatori.
Se il programmatore non e' conscio del problema, non sa dove documentarsi o non ha disciplina nell'applicarsi, e' compito
di qualcun altro correggerlo. Sia il project manager, sia il collega, sia l'architetto di sistema.
Ma se nessuna di queste persone e' capace di correggerlo? Se esse stesse non riconoscono il problema, o lo sottovalutano,
o lo ignorano? Se un architetto manca di conservare la bellezza superficiale, riuscira' a conservare l'integrita' architetturale?
Chiaramente le questioni architetturali sono piu' difficili, ma quant'e' comune riuscire in compiti difficili e non in
quelli facili? E se l'integrita' architetturale non viene conservata neanche dall'architetto (o PM?)
E se non c'e' (piu') un'integrita' architetturale da conservare? E se non c'e' (piu') un architetto?
Dunque, una carenza di bellezza superficiale indica problemi piu' gravi all'interno del progetto.


L'infrazione piu' grave e' sempre la prima.
-------------------------------------------

Cosa succede se ci si trova davanti ad un parco ben tenuto? Generalmente, si tende a conservarne lo stato, quindi a non buttare le
cartaccie in terra e viceversa a tirare i rifiuti nei contenitori.
Ma se lo sporco inizia ad accumularsi, si arriva prima o poi ad un punto critico in cui il parco (o la citta', o il codice...)
non e' piu' percepito come "pulito", ma come "sporco". E allora, chi se ne frega? A cosa serve provare a tenerlo pulito?

Questo punto e' importante eppure sottovalutato. E da' il titolo al post.
Nel suo libro [il punto critico](http://www.amazon.it/critico-effetti-piccoli-cambiamenti-Scienza/dp/8817014303/ref=sr_1_5?ie=UTF8&qid=1326621131&sr=8-5),
Malcolm Gladwell analizza cosa differenzia certi fenomeni insignificanti da certi fenomeni travolgenti, esplorandone
i paralleli con le epidemie (fenomeni `virali`, appunto) e cercandone i fattori chiave.
Uno di questi e' il **Potere del Contesto**, che altro non e' che una manifestazione della [teoria delle finestre rotte](http://en.wikipedia.org/wiki/Broken_windows_theory).

Sebbene se ne possa cogliere una validita' intuitiva, Gladwell dedica ampio spazio nel suo libro (peraltro molto interessante
e altrettanto scorrevole) a cercare conferme e casi concreti a sostegno della validita' di questa teoria.
Uno di questi e' l'epidemia di criminalita' occorsa a New York a cavallo tra gli anni 80 e 90.
Le contromisure per combattere il crimine partirono, anche cronologicamente, dalla metropolitana, una delle zone piu' a rischio
nella citta' per numero di crimini commessi.
Le contromisure prese furono mirate a cambiare il contesto nel quale la gente si muoveva -e quindi commetteva crimini-.
La metropolitana era in uno stato di abbandono sia dal punto di vista degli arredi sia dal punto di vista del controllo
delle regole. I cancelli a pagamento venivano elusi o manomessi per profitto, e tutte le stazioni e i treni erano
danneggiati e/o coperti di graffiti.
Le contromisure furono di istituire tolleranza zero verso questi elementi, relativamente minoritari, di degrado, che pero'
contribuivano in maniera rilevante al clima di abbandono e di noncuranza.
Quindi, un grande sforzo fu investito nel ripulire i vagoni, e anche nel **non** permettere che alcuno ripartisse sporco,
e nel perseguire sistematicamente i reati minori, come il non pagare il biglietto (cifre di uno-due dollari).

Le cronache riportano che queste contromisure, apparentemente velleitarie e fuori contesto, ebbero un impatto determinante.
Naturalmente, non e' possibile dimostrare che fu questo il punto di svolta, data la complessita' dell'ambiente in questione.
Ma il punto e' che certi dettagli "insignficanti" furono oggetto di attenzione e di cura, e questo ebbe un impatto
positivo sull'andamento generale.

Intuitivamente, del resto, se il problema sono gli animali nocivi in un bosco, la cura probabilmente piu' efficacie
non e' tanto attaccare "il problema vero" ovvero gli animali molesti, quanto "un dettaglio secondario" ovvero quell'incuria
nell'ambiente (il contesto) che permette a quegli animali di avere un buon habitat. E quindi di ritornare.

Nel caso della metropolitana e della citta' di New York, sembra "solamente" che questo legame sia piu' nascosto, meno ovvio,
non meno forte. Nel caso di progetti software, e' ancor meno evidente. Ma il filo conduttore esiste, come mostrato sinora.


