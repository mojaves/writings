Title: Progetto tycho #1: il peso delle idee
Date: 2012-03-03 11:36
Tags: italian, Clang, lua, projects, multimedia
Category: projects
Slug: project-tycho-1-the-idea
Author: Francesco Romani
Summary: (Italian) introducing tycho, the bastard heir of transcode.


Intro
-----

Durante il periodo di maintanership di transcode, ci siamo trovati ad affrontare un grossa sfida.
La base di codice era disomogenea, disorganizzata, ridondante. In una parola: in cattivo stato.
Il dilemma che si pose era: ripartire da zero, o quasi, o ripulire in maniera incrementale?
Adottammo la seconda strada, dando cosi', ai tempi inconsapevolmente, ragione a [Joel Spolsky](http://www.joelonsoftware.com/articles/fog0000000069.html).
Il progetto, e con lui la progressiva riscrittura, poi successivamente si areno'. In retrospettiva,
sono ancora convinto che la scelta fosse la migliore. Tuttavia, nel tempo e anche dopo la mia
uscita di scena, ho continuato a raccogliere e raffinare idee su come avrei fatto se si fosse
scelto di ripartire da zero, o quasi. Tutte quelle idee costituiscono il progetto tycho.

<!-- more -->

{% img http://www.lpi.usra.edu/education/timeline/gallery/images/061.jpg 'the tycho crater on the moon' %} 


Il doveroso prologo
-------------------

Per prime le cose futili: il nome `tycho` non significa molto.
Nella realta', e' uno dei crateri della luna e il nome di uno dei pionieri dell'astronomia.
Nel contesto del progetto, e' una parola di senso compiuto che contiene 't' e 'c' (cosi'
facendo si puo' conservare il prefisso `tc` o il namespace `tc` :) ), e richiama la luna. Il che e' interessante
visto che [lua](http://www.lua.org) e' una parte fondamentale del progetto.

[Come iniziato a descrivere altrove](http://mojaves.github.com/blog/2011/03/13/multimedia-processing-in-the-multicore-era/),
tycho parte da transcode, ma non vuole essere un transcode 2.0. Si vira decisamente verso `avisynth`, si abbandonano
alcuni concetti cardine di transcode e se ne introducono di nuovi.

Del transcode originale rimangono una manciata di componenti chiave, [filtri](https://github.com/mojaves/tycho-plugins)
e librerie, e tutta l'esperienza maturata.

Riprendo il discorso iniziato nel post precedentemente linkato, che rimane per motivi storici. Affrontero' singolarmente
i singoli aspetti che nella mia visione hanno affossato transcode, gli errori commessi (o comunque gli aspetti migliorabili)
e come potevano essere risolti, con particolare riferimento all'approccio che dovrebbe essere preso in tycho.


Superficie di attacco
---------------------

Il primo problema, il piu' facile da notare e da misurare, riguarda semplicemente la mera la dimensione
della base di codice. Queste sono all'incirca le statistiche di `transcode 1.1.7`

```
SLOC	Directory	SLOC-by-Language (Sorted)
34512   import          ansic=34512
28835   filter          ansic=28309,pascal=526
10083   src_top_dir     ansic=10083
9400    export          ansic=9400
6528    aclib           ansic=6483,perl=45
6173    libtc           ansic=6173
5739    tools           ansic=5739
5079    testsuite       ansic=3520,perl=1447,sh=112
3871    avilib          ansic=3871
3622    docs            xml=2849,sh=256,php=191,python=144,ansic=139,sed=43
3241    pvm3            ansic=3241
3126    encode          ansic=3126
1195    libtcvideo      ansic=1195
685     multiplex       ansic=685
262     libtcaudio      ansic=262
190     libdldarwin     ansic=190
0       top_dir         (none)


Totals grouped by language (dominant language first):
ansic:       116928 (95.42%)
xml:           2849 (2.32%)
perl:          1492 (1.22%)
pascal:         526 (0.43%)
sh:             368 (0.30%)
php:            191 (0.16%)
python:         144 (0.12%)
sed:             43 (0.04%)




Total Physical Source Lines of Code (SLOC)                = 122,541
[...]
SLOCCount, Copyright (C) 2001-2004 David A. Wheeler
SLOCCount is Open Source Software/Free Software, licensed under the GNU GPL.
SLOCCount comes with ABSOLUTELY NO WARRANTY, and you are welcome to
```

Tradotto: circa 110k righe di codice che interagiscono con una quantita' di formati e librerie diverse,
con un esplosione di API da padroneggiare, ChangeLog da seguire, test da effettuare.

La soluzione di tycho e': ridurre il perimetro. Ridurre *drasticamente* il perimetro.

tycho, come progetto, si focalizza sull'architettura, l'uso (piu') efficiente dei core disponibili e la scriptabilita'.
tycho, come software, si incarna quindi abbastanza naturalmente come strumento di filtraggio, con eventualmente un leggero strato
di decodifica per maggiore praticita'. 

La presenza di due importanti, ingombranti e delicati strati di import ed export non solo non aggiunge nulla
al progetto, ma anzi lo incatena pesantemente. In transcode, questi due strati contribuivano rispettivamente
come primo e quarto elemento per peso. Rinunciandovi, si eliminano circa 45k righe di codice in un solo colpo.

La prima semplificazione cardine e' quindi di standardizzare l'I/O diventa mediante un solo insieme ben
determinato di formati (RAW/PCM/MKV, allo stato attuale).

L'obiettivo, quindi e' avere e _mantenere_ una base di codice ridotta, compatta e densa, gestibile facilmente
da pochi sviluppatori, eventualmente uno solo. tycho si concentra sul produrre un motore di filtering
scriptabile, ortogonale e compatto.
Azzardando una stima a partire dai numeri sopra presentati, e considerando che tycho sfruttera' parte delle
librerie di transcode, l'obiettivo si pone da qualche parte attorno alle 25k righe di codice.


Una digressione sulla manutenibilita'
-------------------------------------

Tutto questo da il "la" per un principio di riflessione. Quanto la qualita' della base di codice controbilancia
il peso delle dimensioni? Esperienze di vita vissuta suggeriscono relazioni degne di ulteriori analisi.
Al culmine (quantitativo) dello sviluppo, la base di codice del mai-rilasciato transcode 1.2.0 consisteva
in poco piu' di 100k righe (si, avevamo consolidato circa il 15% della base di codice, e c'era spazio per
ulteriori miglioramenti

Nella stessa fase, transcode veniva sviluppato con un insieme di tool che gia' nel 2005 veniva considerato da parecchi
drammaticamente primitivo: screen, vim+ctags, autotools + make, gcc + gdb, valgrind.
(Per un approfondimento sulla toolchain unix e di vederlo come IDE-tutto-intorno-a-te, si veda [questa eccezionale serie](http://blog.sanctum.geek.nz/unix-as-ide-introduction/))

Ho esperienza diretta di progetti *piu' piccoli* e di complessita' funzionale paragonabile (maggiore o minore, ma comunque
dello stesso ordine di grandezza, sebbene in ambiti completamente diversi). Parliamo di basi di codice di circa 75k righe,
portati avanti con tool considerati "lo stato dell'arte": IDE full-featured, autocompletamento del codice, ambienti
grafici omnicomprensivi.

Sinora, non e' una vittoria schiacciante.

Naturalmente questa e' considerazione puramente qualitativa, senza numeri a supporto (non mi vengono in mente modi facili
per ottenerne di significativi, e neanche di mediamente facili).
Non e' campanilismo, per quanto la piattaforma *nix con relativa toolchain mi vada sicuramente (e notoriamente) piu' a genio.
Il fatto e' che gli strumenti "moderni" sono sicuramente utili, ma la loro utilita' e' manifesta di fronte
a basi di codice che non e' agevole, o fattibile, tenere in mente. Basi di codice grandi, sparse, disomogenee, ignote.

La tesi non e' che gli strumenti evoluti non servano. Servono *eccome*. Servono talmente che sono ubiquitari.
Il punto e' proprio questo, tuttavia.

Qualunque (nuovo) sviluppatore da per scontata la presenza di IDE+RAD full featured sul suo desktop, fattore alimentato
(e che a sua volta alimenta) la monocultura windows-centrica dello sviluppo. Questo da un lato crea un preconcetto
verso le toolchain diverse, a torto o a ragione considerate inferiori e scomode, dall'altro alimenta inevitabilmente la
pigrizia e la scarsa cura per la bonta' della base di codice.

Si puo' avere una API i cui nomi sono inconsistenti o fuorvianti, tanto c'e' l'autocompletamento a suggerirli.
Si puo' abusare dell'overloading, tanto ci sono i suggerimenti dell'IDE.
Si puo' abusare dell'ereditarieta', tanto abbiamo class diagram autogenerati.

E cosi' via. Tutta roba vista davvero, non teorie.


Di necessita' virtu'
--------------------

Naturalmente non si deve gettare il bambino con l'acqua sporca. Non sto dicendo di rinunciare ai tool moderni, non sto
dicendo che tutti dovrebbero lavorare con vim+screen+ctags, ne che io stesso non beneficio di questi strumenti.

La tesi e' diversa: *non tutto il passato e' obsoleto* e *non si deve abusare delle nuove potenzialita'*.

L'utilizzare toolchain diverse e si, per certi versi piu' primitive (non ho mai avuto voglia di implementare uno
script di autocompletion in vim) aiuta, per effetto collaterale, la bonta' della base di codice.

Non potendo fruire dell'autocompletamento, si deve investire cura nell'avere API minimali, ortogonali, e regolari.
Un beneficio indubbio anche in una monocultura microsoft, dove fuori da visual studio non esiste nulla.

Non potendo avere deployment automatico (o debug interattivo), ci si deve affidare a tool di terze parti,
il che rilassa un vincolo, elimina una dipendenza, e dopo lo scalino iniziale rende piu' agevole *e piu' automatizzabile*
il deployment.

Non potendo facilmente beneficiare di debug interattivo comodo e veloce, si deve insistere su unit test e una buona
infrastruttura di logging.

E cosi' via. Si fa di necessita' virtu', e riemergono tutta una serie di qualita' utili e per il progetto nel suo
complesso.


Piuttosto che frenare, accelera
-------------------------------

Tutto questo non vuol dire che bisogna luddisticamente tornare alle origini. Personalmente sono sempre alla ricerca
di *nuovi* tool o *nuovi* modi per semplificarmi la vita, nel contesto tuttavia che ritengo piu' comodo.
Contesto che personalmente continua ad essere la toolchain stile unix, ma _moderna_, sebbene (con forse uso improprio del termine)
tradizionale.

Non credo nell'approccio "bere o affogare", e non sto suggerendo che al fine di riportare in primo piano quelle
qualita' di omogeneita', regolarita' e integrabilita' si debbano buttare al secchio gli IDE moderni.

Al contrario, penso che occorra farne *maggiore* uso. Sfruttarne le semplificazioni e le velocizzazioni per
*guadagnare tempo e risorse* per *curare maggiormente* gli aspetti che si e' costretti a fare emergere in
loro assenza.

Del resto, tutti gli IDE moderni che conosco (incluso Visual Studio) permettono build non-interattive, integrabilita'
con terze parti etc. etc.
Il danno non e' lo sviluppo, e' la monocultura. In ambiente linux/BSD, la monocultura e' praticamente inesistente
vista la tradizione e la necessita' di integrare strumenti diversi per ottenere il risultato finale.

In altri ambienti, prevalentemente corporate, puo' generare mostri. Ma e' proprio sfruttando tutti gli strumenti
disponibili al massimo, ed eventualmente [cercando di migliorarli ancora](http://blog.reverberate.org/2012/03/02/microsoft-please-support-at-least-a-tiny-bit-of-c99/)
che credo si possa combattere.

#EOF


