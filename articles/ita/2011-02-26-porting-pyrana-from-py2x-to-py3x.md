Title: Porting pyrana from py 2.x to py 3.x
Date: 2011-02-26 21:41
Tags: italian, python3, python, Clang, porting, projects, multimedia, pyrana
Category: python
Slug: porting-pyrana-from-py2x-to-py3x
Author: Francesco Romani
Summary: (Italian) notes about porting a C extension from py 2.x to py 3.x.


Intro
-----

This post is about porting a python C extension module from python 2.x/2.5 to python 3.x/3.2.
It is not a tutorial nor a recipe, a checklist or an HOWTO because I am learning about python 3,
the API and the porting process as I do, so the informations included here can be misleading,
not factual, suboptimal or just plain wrong.
It is more a memorandum and a journal, and for that reason it is written in Italian (my native language).
For those reasons, translation and/or diffusion of this content is NOT encouraged.

<!-- more -->

Recommended readings:

- [the porting of psycopg2](http://mail.python.org/pipermail/python-porting/2008-December/000010.html)
- [porting a C extension module to python 3](http://rhodesmill.org/brandon/2008/porting-a-c-extension-module-to-python-30/)
- [porting extension modules to py3k](http://wiki.python.org/moin/PortingExtensionModulesToPy3k)


Introduzione
------------

Questo post descrive i miei appunti riguardo il porting di [pyrana](http://github.com/mojaves/pyrana)
da python 2.x (2.5) a python 3.x (3.2).
Pyrana e' un modulo di estensione C che permette di manipolare file multimediali, basato su
[ffmpeg](http://ffmpeg.org) e vagamente ispirato a [pymedia](http://pymedia.org).
Visto che sto studiando le nuove API e in generale il  mondo py3 mentre procede con il porting,
questo non e' da intendersi come HOWTO, vademecum o checklist, ma come semplici appunti di viaggio.

Per seguire questi appunti serve una conoscenza base del C (ovviamente) e un minimo di familiarita'
con le API di CPython. La semplice lettura del [tutorial sulle estensioni](http://docs.python.org/py3k/extending/index.html)
puo' essere sufficiente.


Limited API (PEP384), e creazione tipi
--------------------------------------

Uno dei motivi fondamentali per il porting a py3k e' la [PEP 384](http://www.python.org/dev/peps/pep-0384/),
ovvero la disponibilita' di una ABI stabile per le estensioni C. Naturalmente, pyrana e' e resta opensource,
ma la volonta' di mantenere una ABI stabile indica una dedizione ed un interesse da parte dei maintainers di
rendere la vita piu' facile agli sviluppatori di moduli esterni. Questo funge naturalmente anche da incentivo
allo sviluppo (e al porting, per quanto il branch 2.x di python e' praticamente in stasi).

Dal punto di vista operativo il processo e' lineare: la dichiarazione del `PyTypeObject` sparisce,
sostituita da `PyType_Slot` e `PyType_Spec`, piu' la chiamata a `PyType_FromSpec`. Il tutto sin qui e'
facile e ben documentato anche nella PEP.

Devo rilevare due mancanze/bug nella PEP, pero'. Uno lieve, uno piu' serio.
Il primo e' l'incongruenza nel modo di specificare la docstring. PEP alla mano, la docstring dovrebbe
essere un campo della `PyType_Spec`.


	typedef struct{
	   const char* name;
	   const char* doc;
	   int basicsize;
	   int itemsize;
	   int flags;
	   PyType_Slot *slots; /* terminated by slot==0. */
	 } PyType_Spec;


(dalla PEP)


	 typedef struct{
	   const char* name;
	   int basicsize;
	   int itemsize;
	   int flags;
	   PyType_Slot *slots; /* terminated by slot==0. */
	} PyType_Spec;


(da `/usr/include/python3.2/object.h`)

Header alla mano pero', i conti non tornano, e il campo manca.
La soluzione all'enigma lo danno ancora gli header: la docstring e' uno slot (`Py_tp_doc`).
Poco male, e anche piu' regolare.

Il secondo punto e' piu' delicato, e anche piu' oscuro. I sorgenti di python includono
utili esempi, tra cui `Modules/xxlimited.c`, che appunto e' un template di modulo che usa
la API limited di cui alla PEP384. La dichiarazione dei tipi sembra lineare:


	PyMODINIT_FUNC
	PyInit_xxlimited(void)
	{
	    /* ... */
	
 	    Xxo_Type = PyType_FromSpec(&Xxo_Type_spec);
	    if (Xxo_Type == NULL)
	        goto fail;
	
	     /* Create the module and add the functions */
	     m = PyModule_Create(&xxmodule);
	
	    /* ... */

	    return m;
	  fail:
	    Py_XDECREF(m);
	    return NULL;
	}

Manca pero' la chiamata, prima necessaria, a `PyType_Ready`. Una veloce ispezione nei sorgenti
(`Objects/typeobject.c`) conferma che `PyType_FromSpec` NON chiama implicitamente ne automaticamente
`PyType_Ready`. Il che, dato il cambio di API, ci potrebbe anche stare, sebbene poi nell'esempio
di estensione (`noddy`) si trova:


	PyMODINIT_FUNC
	PyInit_noddy4(void) 
	{
	    PyObject* m;
	
	    if (PyType_Ready(&NoddyType) < 0) /* <<<< eccolo qui */
	        return NULL;
	
	    m = PyModule_Create(&noddy4module);
		    if (m == NULL)
	        return NULL;
	
	    Py_INCREF(&NoddyType);
	    PyModule_AddObject(m, "Noddy", (PyObject *)&NoddyType);
	    return m;
	}


SENZA invocare `PyType_Ready` dopo `PyType_FromSpec` tutto SEMBRA funzionare, salvo poi avere
errori casuali e spiazzanti (es: attributi mancanti) in esecuzione.
Reintrodurre la chiamata `PyType_Ready` e' bastato a sanare questa situazione, senza apparenti
effetti collaterali. La questione comunque rimane aperta.



Buffer Protocol, con le buone o con le cattive
----------------------------------------------

L'effetto collaterale indesiderato di usare la Limited API e' la rinuncia al Buffer Protocol,
che non fa parte della suddetta API perche' certi dettagli non son stati finalizzati in tempo;
altri dettagli [qui](http://bugs.python.org/issue10181). Il Buffer Protocol dovrebbe rientrare
nella Limited API in tempo per python 3.3, ma per pyrana nel frattempo serve
una pezza.

La soluzione ad interim si articola in due fasi:

* predisporre il codice ad usare la Limited API ma senza di fatto usarla (`Py_LIMITED_API`
non definito in `setup.py`).
* iniettare esplicitamente il descrittore di protocollo nel tipo creato dinamicamente.


L'iniezione esplicita del descrittore e' basata su questo hack:


	void
	PyrInjectBufferProcs(PyObject* obj, PyBufferProcs* procs)
	{
	    PyHeapTypeObject* h = (PyHeapTypeObject* )obj;
	    h->ht_type.tp_as_buffer = procs;
	    return;
	}


Trovare lo slot giusto in cui iniettare le `PyBufferProcs` ha richiesto un certo sforzo.
Alla fine la soluzione banale si e' rivelata la migliore: e' bastato mimare il piu' fedelmente
possibile quanto gia' fa `PyType_FromSpec` e puntare gli slot di `ht_type`
(e non di `ob_type` come mi ero incaponito a fare).

Esempio di uso del suddetto hack:


	int
	PyrPacket_Setup(PyObject* m)
	{
	    int ret = -1;
	    Packet_Type = PyType_FromSpec(&Packet_Spec);
	    if (Packet_Type) {
	        PyrInjectBufferProcs(Packet_Type, &Packet_AsBuffer);
	        PyType_Ready((PyTypeObject* )Packet_Type);
	        PyModule_AddObject(m, PACKET_NAME, Packet_Type);
	        ret = 0;
	    }
	    return ret;
	}


Fa schifo? Abbastanza. Ma sembra anche funzionare.

In generale il nuovo Buffer Protocol [qui la PEP3118 che lo descrive](http://www.python.org/dev/peps/pep-3118/)
mi piace molto di piu' del vecchio. La API e' piu' lineare, ed effettivamente da quanto visto sinora
sembra ANCHE piu' flessibile. Inoltre, pyrana si inserisce piuttosto bene nei casi d'uso previsti per
la nuova API. Il porting del codice sinora e' stato lineare e non ha riservato sorprese.


Modello di I/O
--------------

Per spiegare le modifiche apportate al modello di I/O di pyrana col porting a python3, occorre prima introdurre
i concetti generali di funzionamento e il precedente schema di funzionamento.

In pyrana solo due oggetti necessitano di effettuare I/O: `pyrana.format.Demuxer` e `pyrana.format.Muxer`.
Per questioni di flessibilita', entrambi delegano l'effettivo I/O ad un oggetto cui ottengono un riferimento
mediante il costruttore.

In pyrana per python2 (= pyrana2), questo oggetto era semplicemente un `file-like`.
La API per i file (ma in generale il modello di I/O per i file) in python <= 2.5 e' molto semplice, e gli oggetti
di tipo file permettono di accedere alla struttura `FILE` sottostante.
Questo si adatta(va) molto bene alle esigenze di pyrana e sopratutto di `libavformat`.
libavformat permette di installare protocolli arbitrari per l'I/O. Sfruttando questo meccanismo
si installa del codice di raccordo per redirigere le richieste di I/O di libavformat al gestore fornito tramite
lo strato python. In effetti, l'approccio di pyrana2 era un po' semplicistico, ma sufficiente in prima battuta.
L'implementazione del codice di raccordo, chiamato con poca fantasia `python file protocol` risiede in
`pyrana/format/pyfileproto.{h,c}`.
Uno schema esemplificativo del modello di I/O in pyrana2 e' presentato nella seguente ASCII-art.

	
	         +--------------------------------+
	         |         Python <= 2.5          |
	         +--------------------------------+
	                         |  object
	                         V interface
	                  +--------------------+
	                  |       pyrana       |<-----+  {{1}}
	                  +--------------------+      | (@init)
	                               |  API         | acquire
	                               V calls        |   I/O
	                      +--------------------+  |  handle
	                      |    libavformat     |--+
	                      +--------------------+
	                                 |   I/O (pyfileproto)
	                                 V  calls   {{2}}
	         +------------------------------------------+
	         |              OS (via libc)               |
	         +------------------------------------------+
	
	
La difficolta' principale in questo schema risiede in una limitazione della API di URLProtocol di libavformat
che NON prevede il passaggio di un dato opaco:

	typedef struct URLProtocol {
	    const char *name;
	    int (*url_open)(URLContext *h, const char *url, int flags);
	    /* ...*/
	} URLProtocol;	

(`/usr/include/libavformat/avio.h`)

Nell'handler di protocollo, la funzione `open` necessita di impostare un riferimento all'oggetto python passato
da poter successivamente riusare per l'accesso al `FILE` sottostante. Questo passaggio va necessariamente
fatto implicitamente mediante un (piccolo) hack. Tutto questo realizza il passo {{1}} nello schema precedente.

Si usa un mapping implicito (e, ahime, globale) nascosto tra nomi di stream pyrana (=`filekey`) e relativi oggetti
python, implementato tramite `g_file_map` in `pyrana/format/pyfileproto.c`. Gli oggetti che devono fare I/O

1. richiedono una `filekey` a `pyfileproto` (ovviamente la responsabilita di gestire il keyspace si delega a
   `pyfileproto`.
2. registrano l'oggetto nel `pyfileproto` usando la chiave ottenuta.
3. a questo punto si puo' procedere all'apertura vera e propria dell'`URLProtocol` di `libavformat`. La `URL`
   che si richiede di aprire contiene sia l'identificatore di protcollo di pyrana, sia la chiave di cui al punto 1.
4. il gestore di protocollo di pyrana puo' a questo punto accedere alla `g_file_map` e recuperare finalmente
   il riferimento all'agognato oggetto, e assegnarne un riferimento nell'`URLContext`.

Una volta a regime (passi {{2}}), le callback di I/O installate dai protocolli altro non fanno che 
recuperare il riferimento al `FILE` ottenuto nel passo {{1}} ed effettuare I/O diretto usando
`fread`, `fwrite`, `fseek`. A regime, dunque, il costo per una operazione di I/O in pyrana2 e' molto contenuto.

Con la [PEP3116](http://www.python.org/dev/peps/pep-3116/), python 3.x e
[python >= 2.6](http://docs.python.org/whatsnew/2.6.html#pep-3116-new-i-o-library)
le cose cambiano.

Il primo cambiamento notevole e' che la API gli oggetti `file-like` si riduce praticamente ad una
forma vestigiale. Data la ricchezza della nuova libreria di I/O, la strategia migliore sembra quella
di richiedere dinamicamente agli oggetti di effettuare le operazioni di I/O.

Operativamente, le operazioni di `binding` di cui al punto {{1}} del funzionamento di pyrana2 rimangono
valide, con la variante che adesso si deve memorizzare direttamente un riferimento a `PyObject`, e non
al `FILE` sottostante.

La storia cambia pesantemente per le operazioni di I/O, che in pyrana3 devono necessariamente passare
per `PyObject_CallMethod`, rivelandosi dunque, in teoria, di costo significativamente maggiore.
Mancando ogni numero a conforto/detrimento di questa teoria, per ora il punto rimane aperto. Al netto
di eventuali future ottimizzazioni, la struttura di base descritta rimane valida, almeno sino ad eventuali
estensioni che verranno eventualmente introdotte da Python 3.3 (e successivi).
Quest'altra ASCII-art esemplifica il modello di I/O di pyrana3.
	
	
	         +--------------------------------+
	         |         Pythin >= 3.2          |<----+
	         +--------------------------------+     |
	             |           |  object              |
	             |           V interface      thin  |
	             |    +--------------------+ mapper |
	             |    |       pyrana       |<------>+   I/O
	             |    +--------------------+        | requests
	             |                 |  API           |  {{*}}
	             |                 V calls          |
	             |        +--------------------+    |
	             |        |    libavformat     |----+
	             |  I/O   +--------------------+
	             | calls
	             V
	         +------------------------------------------+
	         |              OS (via libc)               |
	         +------------------------------------------+
	
	
Una ulteriore complicazione deriva dal fatto che la API di `URLProtocol` specifica il numero di
byte richiesti ad una singola operazione di I/O.
Per conciliare questa esigenza con la libreria di I/O di python3 servono altri due strati (per
ogni chiamata di I/O).

1. un leggero strato a base di `Py_buffer` per adattare i parametri tra le due API
2. uno strato piu' pesante con un oggetto `PyMemoryView` creato al volo per OGNI richiesta di I/O.

C'e' naturalmente spazio per le ottimizzazioni in questo punto (per esempio l'oggetto
`PyMemoryView` puo' con tutta probabilita' essere messo in qualche cache), ma questo sara'
oggetto di futuro studio.


Stringhe VS Unicode/Byte
------------------------

Il ciclone UNICODE ha lasciato pyrana relativamente indenne, e ha anzi contribuito a razionalizzare
un po' il codice. Semplicemente, in tutti i punti in cui pyrana necessita di produrre stringhe vere
e proprie (descrizioni, `repr`), si passa da `PyString` a `PyUnicode`.
Dove si lavora con byte grezzi, si passa da `PyString` a `PyBytes` (occasionalmente a `PyByteArray`).
Lineare, quasi al punto di far bastare una botta di `sed` per sistemare la questione.


Nuova API per getter e setter
-----------------------------

No c'e' molto da dire, a parte un cambio di prototipo di cui ancora non ho apprezzato l'utilita'.
Tutto molto lineare.


	
	static PyObject *
	PyrPacket_GetSize(PyrPacketObject *self, void *closure)
	{
	    return PyLong_FromLong(self->pkt.size);
	}



Creazione moduli
----------------

Anche in questo caso, come per i `getter`/`setter`, il cambiamento c'e' ma niente di sconvolgente.
A mio personalissimo gusto, la creazione di moduli diventa piu' regolare e uniformata con quella,
per esempio, dei tipi.


	
	static struct PyModuleDef pyranaformatmodule = {
	    PyModuleDef_HEAD_INIT,
	    FORMAT_SUBMODULE_NAME,
	    FORMAT_SUBMODULE_PYDOC,
	    -1,
	    FormatFunctions,
	    NULL,
	    NULL,
	    NULL,
	    NULL
	};
	
	int
	PyrFormat_Setup(PyObject *m)
	{
	    int ret = -1;
	    PyObject *sm = PyModule_Create(&pyranaformatmodule);
	    if (sm) {
	        PyrFileProto_Setup();
	
	        g_input_formats  = BuildFormatNamesInput();
	        g_output_formats = BuildFormatNamesOutput();
	
	        PyModule_AddIntConstant(sm, "STREAM_ANY", Pyr_STREAM_ANY);
	        PyModule_AddObject(sm, "input_formats", g_input_formats);
	        PyModule_AddObject(sm, "output_formats", g_output_formats);
	
        	PyrPacket_Setup(sm);
	        PyrDemuxer_Setup(sm);
        	PyrMuxer_Setup(sm);
	
        	PyModule_AddObject(m, "format", sm);
	        ret = 0;
	    }
	    return ret;
	}



# EOF
