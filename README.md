writings
========

blog articles, essays, rants and notes. All written in [markdown](http://en.wikipedia.org/wiki/Markdown).

While I try to write mostly in english, some writing are intentionally in Italian (my native language).
Most of those writings are made available on my [blog](http://mojaves.github.com) (find them under articles/).

Unless otherwise specified, all the content is licensed under the [creative commons Attribution-NoDerivs 3.0 Unported license](http://creativecommons.org/licenses/by-nd/3.0/).

