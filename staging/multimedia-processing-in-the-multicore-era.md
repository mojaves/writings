---
layout: post
title: "multimedia processing in the multicore era"
date: 2011-03-13 21:43
comments: false
categories: [english, Clang, projects, multimedia, transcode]
---

As my first significant contribution to the opensource ecosystem, I served as
maintainer for [transcode](http://tcforge.berlios.de) package for quite a few years.
transcode is a multimedia processing software which since the beginning made extensive use
of multithreading, thus exploiting any hardware concurrency.
Unfortunately, when transcode was most popular (circa 1999-2002), a multiprocessor machine was
very rare in the consumer mass market (the only one I had access at the time).

<!-- more -->

Even as maintainer, I almost never got transcode to run on a multiprocessor/core box.
When I developed transcode, I had a monoprocessor box; those days I enjoy a multicore
box, but I do not develop transcode anymore. I simply lost much of my interest in the area,
and since I firmly believe in the 'eat your own dogfood' motto, I choose to step back.
Unfortunately, looks like noone took my place so far.

The unfortunate sequence of events described above always let me a bit unsatisfied.
Among the reasons I had to working on transcode, the most significant was
the overall architecture, featuring pervasive usage of threads and plugins.

So, as time passed and I got different software to deal with, and a bit more experience with
hardware concurrency, the unsatisfaction never disappeared entirely.

When I started writing down this essay, I planned to collect all lesson learned and the
experience made with transcode. Time passed, I found myself stuck in writing (I'm not good
at blogging). Now I'm revamping this essay, with a new target.

I'd like to write down my ideas about a long-planned replacement for transcode. I've
made  rumblings about this for a while, and discussed too on transcode-devel back at the times.
The ideas and principles outlined here will unlikely ever be materialized in code
for the usual reasons, but I think they deserve some better organization and exposure.


The concept(s) (aka TL;DR aka the summary)
------------------------------------------

The New Transcode should embody the following concepts:


* multicore friendly(ier);

* stronger focus on processing (filtering) than transcoding (decoding/encoding);

* consistent configuration;

* scriptable: configuration should be express by a script;

* scriptable: the filte themselves should be made as a script;

* scriptable: using a REAL, well known, scripting language;

* pluggable: real, strong plugin based API (transcode was a mess with no formal API);

* a catchy name (oh, that was easy: 'tycho');


Something like [avisynth](http://www.avisynth.org), but NOT an avisynth clone.


The concepts in depth
---------------------

The core concepts behind the new transcoding engine are thus three:
better concurrency (both as in easier and in more effective), better plugins and scripting.
The remainder of the document expand those concepts. Since the aspetcs are intertwined,
the division is not strict and differenct concepts can (and will) pop out in the
sections entitled to others.


Script everywhere
-----------------

This is mostly a pet peeve of mine. However, having a built-in, powerful scripting support
helps the whole project staying consistent.

Multimedia processing tools exposes *a lot* of options and tunables, sometimes hidden
under layer of configurations, and/or frozen into presets (maybe recipes, maybe embodied
in the UI itself, maybe defaults, whatever). Almost everyone which does not want, or can,
use a simplified UI find himself writing a script sooner or later. A shell script.

So, the tool has to expose a lot of options, often using a quirky and inconsistent syntax.
The script has to access those settings, and the juggling between the shell and the tool
can become annoying sometimes. Quite rarely the tool/library/framework (but in all honesty
the vast majority of processing is done by wrapping the FFMpeg *binary* or with a -stock-
gstreamer pipeline) exposes hooks to a better scripting language.

There is an impedence between the script and the engine. It is not the most annoying let alone
the hardest problem out there, but compare this with the discoverability and friendliness
of a proper REPL of a proper language like ruby or python.

In a greenfield project, like tycho, it is possible to make things easier by blending
the processing engine with the scripting support.

This does not involve just configuration, of course. On the other hand, it is nice to have
the possibility to use just a script as a processing node. avisynth let do that and this
is extremely cool. On the other hand, the script has to offer "acceptable" performance.

Those days, while still wondering what is "acceptable", we can choose between very fast
and nice execution engines. Just a few comes to mind in zero time:

* V8 (JavaScript)
* Jager/Trace/Ion/WhateverIsLastNowMonkey (JavaScript)
* LuaJIT (Lua)


to be continued...

