---
layout: post
title: "progetto tycho #2: scopo del gioco"
date: 2012-08-05 10:25
comments: false
published: false
categories: [italian, Clang, lua, projects, multimedia]
---

Transcode incorporava una serie di caratteristiche architetturali interessanti e forse
in anticipo sui tempi, tali che lo rendevano attraente ai miei occhi anche sotto il profilo
geek, non sotto quello di mero utilizzatore. In particolare erano notevoli nel
2003: l'architettura nativamente e intensivamente multithread; la divisione in strati ben
definiti; l'uso pervasivo di plugin. Purtroppo, transcode ha sempre drammaticamente sofferto
della carenza di risorse di sviluppo. Da che ricordi, quello che era (informalmente)
il core team e' stato al massimo composto da due persone; il numero di contributori
era stabile ma ridotto (tre, quattro sviluppatori). Nulla di paragonabile ad altri progetti
come `mplayer`/`mencoder` o `ffmpeg`, e quindi le buone idee introdotte da transcode
sono state presto adottate anche altrove, mentre le cattive idee sono rimaste.

<!-- more -->


Il modello di threading
-----------------------

Inizialmente l'uso pervasivo di thread sembrava una buona idea, anche se nell'eta' dell'oro
di transcode le macchine multiprocessore scarseggiavano, e il multicore era irraggiungibile.
Purtroppo come dicono talvolta la bellezza puo' essere superficiale, mentre la bruttezza puo'
arrivare sino all'osso. Ad un piu' attento esame, sorge il dubbio se la ripartizione in thread
adottata da transcode sia veramente efficiente.


### Architettura generale

L'architettura generale di transcode con riferimento agli stadi di elaborazione e alla ripartizione
in thread e' [schematizzata in questa pagina](http://www.transcoding.org/transcode?Transcode_Internals/A_Video_Frame_In_Transcode).
Il titolo si riferisce ad un frame video, ma la gestione di un frame audio e' identica.
Lo schema grafico (?) non e' molto fedele alla realta. Una versione piu' corretta e' la seguente


	               2  I M P O R T  T H R E A D S

	          decode()                       decode()
	             |                              |
	      1) preprocess()                1) preprocess()
	             |                              | 
	2) filters(TC_PRE_S_PROCESS)   2) filters(TC_PRE_S_PROCESS)
	             |                              |
	             |______________________________|
	            /      |        |        |       \
	           /       |        |        |        \
	     N  F R A M E  P R O C E S S I N G  T H R E A D S
	        /          .        .        .          .
	       |           .        .        .          .
	       |
	 3) filters(TC_PRE_M_PROCESS)
	       |
	 4) process_vid_frame()
	       |
	 5) filters(TC_POST_M_PROCESS)
	       |
	       |           .        .        .         .
	        \          .        .        .        .
	         \_________|______  |  ______|_______/
	                          \ | /
	                            |
	               6) filters(TC_POST_S_PROCESS)
	                            |
	                   7) postprocess()
	                            |
	                         encode()

	              1 E X P O R T  T H R E A D


Le perplessita' maggiori che emergono:

* l'applicazione dei filtri puo' avvenire sia in maniera sincrona, ovvero eseguita
  da un thread proposto ad altro (import, export) oppure in maniera parallela,
  ovvero da un thread esclusivaente dedicato al filtraggio.
* la batteria di thread di filtraggio sembra implementare (nel seguito maggiori
  dettagli e la realta') un thread pool, scelta discutibile di per se perche'
  un numero eccessivo di thread provoca inattivita' di parte di essi, un numero
  ridotto causa ovviamente un collo di bottiglia.
* l'encoder, lo stadio tipicamente piu' pesante, e' gestito sempre e solo da un
  thread, che fa anche da punto di sincronizzazione.
* non e' deducibile immediatamente e ovviamente, ma l'intero stadio di filtraggio
  puo' essere escluso implicitamente o esplicitamente. L'utente non ha necessariamente
  il controllo su dove viene applicato un dato filtro, se ad esempio in modalita'
  sincrona o parallela.
* i filtri occupano uno slot fisso e `hardcoded` nella catena di elaborazione.
  l'utente non ha necessariamente la possibilita' di riarrangiare la sequenza,
  ne la ha `transcode`, al fine di ottimizzare le operazioni.

Fatte salve le puntualizzazioni di cui sopra, lo strato di import si puo'
trattare velocemente. Transcode usa un thread per ogni flusso elaborato,
audio e video, *sempre e comunque*, adottando eventualmente una forma
di `Null Pattern` per compensare un flusso mancante (sorgente solo audio
o solo video). Questo implica che la sorgente viene acceduta due volte,
cosa che non e' sempre ottimale (sorgenti `streamed`, DVB).

Un thread di import si occupa di I/O, demuxing, decoding e applicazione di filtri
sincroni. Si tratta della parte che offre le maggiori diffcolta' implementative
perche' deve normalizzare la sorgente di ingresso (teoricamente occupandosi anche
di riprendere desincronie), ma architetturalmente e' la piu' semplice.

Prima di proseguire occorre trattare il modo con cui gli stadi interagiscono,
ovvero il `framebuffer` di transcode.


### Framebuffer: interazione tra stadi


### Strato di filtraggio


### Encoder/Export



L'interfaccia dei plugin
------------------------



Estensibilita'
--------------


Configurabilita'
----------------


#EOF


