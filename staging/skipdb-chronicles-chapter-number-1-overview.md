---
layout: post
title: "SkipDB Chronicles Chapter #1 - Overview"
date: 2012-04-12 11:19
comments: false
published: false
categories: [english, Clang, projects, dbmlike, skipdbchronicles]
---

The [SkipDB DBM-like library](https://github.com/stevedekorte/skipdb) provides
an implementation of an embeddable Key-Value storage by using the SkipLists
as fundamental data structure workhorse instead the ubiquitous B+Tree.
In this article, the library is dissected in order to expose the underlying
architecture and the internal components. Code references and fancy pictures
will follow.

<!-- more -->

The architecture of SkipDB
--------------------------

[Fancy Archictectural Overview picture here]

The architecture of SkipDB is already outlined by the organization of the source code
into the root directory:

```
libs/basekit
libs/jfile
libs/udb
libs/skipdb
```

Shown above, the main components coing rougly from the bottom to the top layer, being `skipdb`
the uppermost (client-visible) layer of the library.
The components interact to each other in a quite layered fashion, so the `skipdb` part uses mostly
the services provided by the `udb` part which in turn relies on `jfile`. The main exception
is the base layer. SkipDB assumes to operate on a POSIX(-like) environment, augmented with the services
provided by the `basekit` part. It's worth noting that while a POSIX(-like) is required,
the I/O model (more on this topic later) is instead mostly based on the C library services.

The purpose of the components of SkipDB is:

* `basekit` provides an array of utility and platform-uniformation functions. Provides common and reliable
   ground much like portable runtime libraries like `glib` `nspr` or `apr`.

* `jfile` provides a simple Journal File implementation based on the C language I/O model

* `udb` manages the raw unordered data/index block-device I/O. It is the real workhorse of the library.
  `udb` relies on `jfile` for resilience.

* `skipdb` make use of `udb` and provides both the indexing model thanks to the SkipLists and the
  client interface.

A description of the above components in greater detail will now follow.


BaseKit: lean and mean portable runtime
---------------------------------------

TODO


JFile: journaled file implementation
------------------------------------

TODO


UDB: on-disk record access
--------------------------

TODO


skipdb: SkipList-based record access
------------------------------------

TODO


Chronicler Postil
-----------------

This post is part of the "SkipDB Chronicles". Lookup the `skipdbchronicles` tag for more chapters.


#EOF

