---
layout: post
title: "gradi di degradazione"
date: 2011-02-26 21:41
comments: false
published: false
categories: [italian, programming, essay, craftmanship]
---

[Il codice bello non dura](http://prog21.dadgum.com/108.html). Un eccellente prototipo,
una buona architettura, un bel disegno, possono diventare sporchi e brutti dopo aver
superato la prova della produzione. E se questo e' il prezzo per arrivarci e per rimanerci,
in produzione, puo' essere un prezzo equo. Dopotutto, fatto e' meglio di perfetto,
a tutti i livelli. Il software va finito, e un cimitero di bei relitti semicompletati
non solleva lo spirito piu' di una galleria di software goffo e sporco ma che funziona
tutti i giorni. La bellezza e la pulizia non sono valori in quanto tali, ma anche essi
*strumenti* al servizio delle altre qualita' percepibili del codice del codice.
Dunque, il destino inevitabile di tutto il software che va in produzione e' quello di
sporcarsi e appesantirsi, e pensare il contrario e' un ninnolo da accademici, da
sviluppatori giovani e da hippy del software?
Non necessariamente, ma la rincorsa parte da lontano.




---------------------------------------------------------------------

http://blog.regehr.org/archives/663 - can simplicity scale
http://www.jwz.org/doc/worse-is-better.html
http://swreflections.blogspot.com/2012/02/technical-debt-how-much-is-it-really.html
http://www.joelonsoftware.com/articles/fog0000000069.html

